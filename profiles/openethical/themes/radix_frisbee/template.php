<?php
/**
 * @file
 * Theme functions
 */

require_once dirname(__FILE__) . '/includes/theme.inc';
require_once dirname(__FILE__) . '/includes/structure.inc';
require_once dirname(__FILE__) . '/includes/field.inc';
require_once dirname(__FILE__) . '/includes/comment.inc';
require_once dirname(__FILE__) . '/includes/form.inc';
require_once dirname(__FILE__) . '/includes/menu.inc';
require_once dirname(__FILE__) . '/includes/node.inc';
require_once dirname(__FILE__) . '/includes/panel.inc';
require_once dirname(__FILE__) . '/includes/block.inc';
require_once dirname(__FILE__) . '/includes/user.inc';
require_once dirname(__FILE__) . '/includes/view.inc';

/**
 * Implements hook_css_alter().
 */
function radix_frisbee_css_alter(&$css) {
  $radix_path = drupal_get_path('theme', 'radix');

  // Radix now includes compiled stylesheets for demo purposes.
  // We remove these from our subtheme since they are already included
  // in compass_radix.
  unset($css[$radix_path . '/assets/stylesheets/radix-style.css']);
  unset($css[$radix_path . '/assets/stylesheets/radix-print.css']);

  radix_frisbee_remove_css($css, 'radix_layouts', 'radix_layouts');
  radix_frisbee_remove_css($css, 'panopoly_theme', 'panopoly-accordian', 'css');
  radix_frisbee_remove_css($css, 'panopoly_theme', 'panopoly-featured', 'css');
  radix_frisbee_remove_css($css, 'panopoly_core', 'panopoly-dropbutton', 'css');
  radix_frisbee_remove_css($css, 'panopoly_core', 'panopoly-fonts', 'css');
  radix_frisbee_remove_css($css, 'twitter_pull', 'twitter-pull-listing');
  radix_frisbee_remove_css($css, 'caption_filter', 'caption-filter'  );

  if(!user_access('access administration pages')){
    radix_frisbee_remove_css($css, 'panopoly_wysiwyg', 'panopoly-wysiwyg');
    radix_frisbee_remove_css($css, 'panopoly_magic', 'panopoly-magic', 'css');
    radix_frisbee_remove_css($css, 'panopoly_magic', 'panopoly-modal', 'css');
    radix_frisbee_remove_css($css, 'views', 'views', 'css');
    radix_frisbee_remove_css($css, 'panels', 'panels', 'css');
    radix_frisbee_remove_css($css, 'date_popup', 'datepicker.1.7', 'themes');
    radix_frisbee_remove_css($css, 'date_api', 'date'  );
    radix_frisbee_remove_css($css, 'ctools', 'ctools', 'css');
    radix_frisbee_remove_css($css, 'user', 'user'  );
    radix_frisbee_remove_css($css, 'search', 'search'  );
    radix_frisbee_remove_css($css, 'node', 'node'  );
    radix_frisbee_remove_css($css, 'panopoly_admin', 'panopoly-admin-navbar'  );
    radix_frisbee_remove_css($css, 'panopoly_theme', 'panopoly-layouts', 'css'  );

    //hmm these too won't budge
    //radix_frisbee_remove_css($css, 'panopoly_core', 'panopoly-jquery-ui-theme', 'css' );
    //radix_frisbee_remove_css($css, 'jquery_update', 'jquery.ui.accordion.min', 'replace/ui/themes/base/minified'  );
  }
}

/**
 * Implements hook_js_alter().
 */
function radix_frisbee_js_alter(&$js) {

  //stop Radix from fiddling round with things. It was breaking dropdowns
  //unset($js[drupal_get_path('theme', 'radix') . '/assets/javascripts/radix-script.js']);

  //we dont' need panopolys spotlight js as we're using the bootstrap carousel
  unset($js[drupal_get_path('module', 'panopoly_widgets') . '/panopoly-widgets-spotlight.js']);

  if(!user_access('access administration pages')){
    radix_frisbee_remove_js($js, 'panopoly_theme', 'panopoly-accordion', 'js');
    radix_frisbee_remove_js($js, 'panopoly_magic', 'panopoly-magic');
    radix_frisbee_remove_js($js, 'panopoly_admin', 'panopoly-admin');
    radix_frisbee_remove_js($js, 'panels', 'panels', 'js');
    radix_frisbee_remove_js($js, 'caption_filter', 'caption-filter', 'js');
    radix_frisbee_remove_js($js, 'jquery_update', 'jquery.ui.accordion.min', 'replace/ui/ui/minified');
    radix_frisbee_remove_js($js, 'jquery_update', 'jquery.ui.tabs.min', 'replace/ui/ui/minified');
  }

  unset($js['misc/tableheader.js']);
}


function radix_frisbee_remove_css(&$css, $module, $file, $directory = ''){

  $file = $file . '.css';
  if($directory != ''){
    $directory = $directory . '/';
  }
  $path = drupal_get_path('module', $module) . '/' . $directory;

  if (isset($css[$path . $file])) {
    unset($css[$path . $file]);
  }
}

function radix_frisbee_remove_js(&$js, $module, $file, $directory = ''){
  $file = $file . '.js';
  if($directory != ''){
    $directory = $directory . '/';
  }
  $path = drupal_get_path('module', $module) . '/' . $directory;

  if (isset($js[$path . $file])) {
    unset($js[$path . $file]);
  }
}

/**
 * Implements theme_menu_tree().
 */
function radix_frisbee_menu_tree__menu_block__main_menu(&$variables) {
  return '<ul class="menu nav nav-list">' . $variables['tree'] . '</ul>';
}

/**
 * Implements theme_menu_link().
 */
function radix_frisbee_menu_link(array $variables) {

  $element = $variables['element'];
  $sub_menu = '';

  if ($element['#below']) {

    unset($element['#below']['#theme_wrappers']);
    $sub_menu = '<ul>' . drupal_render($element['#below']) . '</ul>';

    $element['#localized_options']['attributes']['data-target'] = '#';
  }

  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

/**
 * Implements theme_menu_link().
 * For the main menu when not in the header. same as standard menu link.
 */
function radix_frisbee_menu_link__menu_block__main_menu(array $variables) {

  return radix_frisbee_menu_link($variables);
}

/**
 * Render callback.
 *
 * @ingroup themeable
 *
 * Removes panel separator
 */
function radix_frisbee_panels_default_style_render_region($vars) {
  $output = '';
  $output .= implode($vars['panes']);

  return $output;
}

/*
 * Implements theme_breadcrumb()
 *
 */
function radix_frisbee_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];

  $title = drupal_get_title();

  if ($title && $title != 'Home') {
    $breadcrumb[] = $title;
  }

  if (!empty($breadcrumb) && sizeof($breadcrumb) > 2) {
    // Provide a navigational heading to give context for breadcrumb links to
    // screen-reader users. Make the heading invisible with .element-invisible.
    $output = '<h2 class="element-invisible">' . t('You are here') . '</h2>';

    $output .= '<div class="breadcrumb">' . implode(' / ', $breadcrumb) . '</div>';
    return $output;
  }
}

/**
 * Theme function for the spotlight wrapper
 */
function radix_frisbee_panopoly_spotlight_wrapper($variables){
  $items = $variables['element']['#items'];
  $duration = $variables['element']['#duration'];

  if(empty($duration)) {
    $duration = 8;
  }

   // Assemble the tabs header
  $header = '<div id="myCarousel" class="carousel slide" data-interval="' . $duration * 1000 . '" data-ride="carousel">';
  $header .= '<ol class="carousel-indicators">';

  foreach ($items as $delta => $item_data) {
    if($delta > 0){
      $navClass = "";
    }
    else{
      $navClass = ' class="active"';
    }

    $header .= '<li ' . $navClass . ' data-target="#myCarousel" data-slide-to="' . $delta . '"></li>';
  }

  $header .= '</ol>';
  $header .= '<div class="carousel-inner" role="listbox">';

  // Assemble the tabs footer
  $footer = '</div><a class="carousel-control left" href="#myCarousel" data-slide="prev" role="button"><i class="glyphicon glyphicon-chevron-left" aria-hidden="true"></i><span class="sr-only">Previous</span></a>';
  $footer .= '<a class="carousel-control right" href="#myCarousel" data-slide="next" role="button"><i class="glyphicon glyphicon-chevron-right" aria-hidden="true"></i><span class="sr-only">Next</span></a>';
  $footer .= '</div>';

  return $header . render($variables['element']['slides']) . $footer;
}

  /**
 * Theme function for spotlight items
 */
function radix_frisbee_panopoly_spotlight_view($variables) {

  $title = $variables['element']['#items']['title'];
  $description = $variables['element']['#items']['description'];
  $link = $variables['element']['#items']['link'];
  $alt = $variables['element']['#items']['alt'];
  $settings = $variables['element']['#settings'];
  $delta = $variables['element']['#delta'];

  $image = $variables['element']['#items']['image'];
  $image_markup = theme('image_style', array('style_name' => $settings['image_style'], 'path' => $image->uri, 'alt' => $alt));

  if($delta > 0){
    $class = "item";
  }
  else{
    $class = "item active";
  }

  $output = '<div class="' . $class . '">';
    $output .= $image_markup;
    $output .= '<div class="carousel-caption">';
    $output .= '<h2>' . l($title, $link) . '</h2>';
    $output .= '<p>' . $description . '</p>';
    $output .= '</div>';
  $output .= '</div>';

  return $output;
}

function radix_frisbee_file_icon($variables) {
  $file = $variables['file'];
  $mime = check_plain($file->filemime);

  return '<i class="glyphicon glyphicon-file" aria-hidden="true" title="' . $mime . '"></i>';
}

/**
 * Implements template_preprocess_page().
 */
function radix_frisbee_preprocess_page(&$variables) {

  // Add copyright to theme.
  if ($copyright = theme_get_setting('copyright')) {
    $variables['copyright'] = check_markup($copyright['value'], $copyright['format']);
  }

  // Format and add main menu to theme.
  $variables['main_menu'] = _radix_dropdown_menu_tree(variable_get('menu_main_links_source', 'main-menu'), array(
    'min_depth' => 1,
    'max_depth' => 1,
  ));

  // Format and add secondary menu to theme.
  $variables['secondary_menu'] = _radix_dropdown_menu_tree(variable_get('menu_secondary_links_source', 'secondary-menu'), array(
    'min_depth' => 1,
    'max_depth' => 1,
  ));
}

/**
 * Implements theme_facetapi_link_active
 * Exactly the same except addition of count to link
 */
function radix_frisbee_facetapi_link_active($variables) {

  // Sanitizes the link text if necessary.
  $sanitize = empty($variables['options']['html']);
  $link_text = ($sanitize) ? check_plain($variables['text']) : $variables['text'];

    // Adds count to link if one was passed.
  if (isset($variables['count'])) {
    $variables['text'] .= ' ' . theme('facetapi_count', $variables);
    $link_text .= ' ' . theme('facetapi_count', $variables);
  }

  // Theme function variables fro accessible markup.
  // @see http://drupal.org/node/1316580
  $accessible_vars = array(
    'text' => $variables['text'],
    'active' => TRUE,
  );

  // Builds link, passes through t() which gives us the ability to change the
  // position of the widget on a per-language basis.
  $replacements = array(
    '!facetapi_deactivate_widget' => theme('facetapi_deactivate_widget', $variables),
    '!facetapi_accessible_markup' => theme('facetapi_accessible_markup', $accessible_vars),
  );
  $variables['text'] = t('!facetapi_deactivate_widget !facetapi_accessible_markup', $replacements);
  $variables['options']['html'] = TRUE;
  return theme_link($variables) . $link_text;
}


/*
 * Overriding theme_item_list.
 * Necessary because theme_item_list has a \n after the closing li which is causing firefox problems
 * https://developer.mozilla.org/en-US/docs/Web/Guide/API/DOM/Whitespace_in_the_DOM
 */
function radix_frisbee_item_list(&$variables) {
  $items = $variables['items'];
  $title = $variables['title'];
  $type = $variables['type'];
  $attributes = $variables['attributes'];

  // Only output the list container and title, if there are any list items.
  // Check to see whether the block title exists before adding a header.
  // Empty headers are not semantic and present accessibility challenges.
  $output = '<div class="item-list">';
  if (isset($title) && $title !== '') {
    $output .= '<h3>' . $title . '</h3>';
  }

  if (!empty($items)) {
    $output .= "<$type" . drupal_attributes($attributes) . '>';
    $num_items = count($items);
    $i = 0;
    foreach ($items as $item) {
      $attributes = array();
      $children = array();
      $data = '';
      $i++;
      if (is_array($item)) {
        foreach ($item as $key => $value) {
          if ($key == 'data') {
            $data = $value;
          }
          elseif ($key == 'children') {
            $children = $value;
          }
          else {
            $attributes[$key] = $value;
          }
        }
      }
      else {
        $data = $item;
      }
      if (count($children) > 0) {
        // Render nested list.
        $data .= theme_item_list(array('items' => $children, 'title' => NULL, 'type' => $type, 'attributes' => $attributes));
      }
      if ($i == 1) {
        $attributes['class'][] = 'first';
      }
      if ($i == $num_items) {
        $attributes['class'][] = 'last';
      }
      $output .= '<li' . drupal_attributes($attributes) . '>' . $data . "</li>";
    }
    $output .= "</$type>";
  }
  $output .= '</div>';
  return $output;
}

/**
 * Overriding theme_date_display_range()
 * Exactly the same as overriden function apart from:
 * No div wrapping the result
 */
function radix_frisbee_date_display_range($variables) {
  $date1 = $variables['date1'];
  $date2 = $variables['date2'];
  $timezone = $variables['timezone'];
  $attributes_start = $variables['attributes_start'];
  $attributes_end = $variables['attributes_end'];
  $show_remaining_days = $variables['show_remaining_days'];

  $start_date = '<span class="date-display-start"' . drupal_attributes($attributes_start) . '>' . $date1 . '</span>';
  $end_date = '<span class="date-display-end"' . drupal_attributes($attributes_end) . '>' . $date2 . $timezone . '</span>';

  // If microdata attributes for the start date property have been passed in,
  // add the microdata in meta tags.
  if (!empty($variables['add_microdata'])) {
    $start_date .= '<meta' . drupal_attributes($variables['microdata']['value']['#attributes']) . '/>';
    $end_date .= '<meta' . drupal_attributes($variables['microdata']['value2']['#attributes']) . '/>';
  }

  // Wrap the result with the attributes.
  $output = t('!start-date to !end-date', array(
    '!start-date' => $start_date,
    '!end-date' => $end_date,
  ));

  // Add remaining message and return.
  return $output . $show_remaining_days;
}
