<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */

// Load the file
$file = file_load($fields['fid']->content);
$display['settings'] = array('image_style' => 'medium');

$link = url('file/' . $fields['fid']->content);

if($file->filemime == 'video/youtube'){
  module_load_include('inc', 'media_youtube', '/includes/media_youtube.formatters.inc');
  $image_render_array = media_youtube_file_formatter_image_view($file, $display, LANGUAGE_NONE);
}
elseif($file->filemime == 'video/vimeo'){
  module_load_include('inc', 'media_vimeo', '/includes/media_vimeo.formatters.inc');
  $image_render_array = media_vimeo_file_formatter_image_view($file, $display, LANGUAGE_NONE);
}

$link_split = explode('"', $fields['link']->content);
?>

<a href="<?php print $link_split[1]; ?>"><?php print render($image_render_array); ?></a>