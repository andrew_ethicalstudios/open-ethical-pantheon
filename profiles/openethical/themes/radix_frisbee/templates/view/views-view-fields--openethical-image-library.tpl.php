<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */

$path = $fields['url']->content;
$files_path = "http://$_SERVER[HTTP_HOST]" . base_path() . variable_get('file_public_path', conf_path() . '/files') . '/';
$path = str_replace($files_path, '', $path);
$path = image_style_url('medium', file_build_uri($path));
$alt = $fields['field_file_image_alt_text']->content;
$link = url('file/' . $fields['fid']->content);

?>

<a href="<?php print $link; ?>">
  <img src="<?php print $path; ?>" alt="<?php print $alt; ?>" />
</a>