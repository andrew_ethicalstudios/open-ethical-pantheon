<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */

module_load_include('inc', 'ethical_document_library', 'ethical_document_library.terms');

$sidebar = '';
$class = '';
$terms = '';
$project_terms = '';
$category_terms = '';
$countries = '';

if(empty($fields['field_featured_image']->content)){
    $class = 'no-sidebar';
}
else{
  $sidebar = '<div class="sidebar">' . $fields['field_featured_image']->content . '</div>';
}

// Create a list of links (one for each term), for a each vocabulary
// Each link links to the document library filtered to the term along with any filtering already in place

if(!empty($fields['field_project'])){
  $project_terms = ethical_taxonomy_generate_terms_list($fields['field_project']->content, '||', 'field_project%3Atitle:');
}
if(!empty($fields['field_featured_categories'])){
  $category_terms = ethical_taxonomy_generate_terms_list($fields['field_featured_categories']->content, '||', 'field_featured_categories%3Aname:');
}
if(!empty($fields['field_country_name'])){
  $countries = ethical_taxonomy_generate_terms_list($fields['field_country_name']->content, '||', 'field_country%3Aname:');
}

$terms = $project_terms . $category_terms . $countries;

if(!empty($terms)){
  $terms = rtrim($terms, ', ');
}
?>

<div class="node-list">
  <div class="oe-basic clearfix <?php if (!empty($class)) { print $class; } ?>">
    <?php print $sidebar ?>
    <div class="contentmain">
      <?php if(!empty($fields['filename_field']->content)){ print '<h3><a href="' . $fields['url']->content . '">' . ethical_media_get_title_from_filename($fields['filename_field']->content) . '</a></h3>'; }?>

      <?php if(!empty($fields['field_file_author']->content) || !empty($fields['field_file_published_date']->content)){ ?>
      <div class="meta">
        <?php if(!empty($fields['field_file_author']->content)){ print $fields['field_file_author']->content; }?>
        <?php if(!empty($fields['field_file_published_date']->content)){ print $fields['field_file_published_date']->content; }?>
      </div>
      <?php } ?>

        <?php if(!empty($fields['field_file_description']->content)){ print $fields['field_file_description']->content; }?>
       <?php if(!empty($fields['rendered_entity']->content)){ print $fields['rendered_entity']->content; }?>
      <?php if(!empty($terms)){ print '<div class="tags"><p>' . t('Tags: ') . $terms . '</p></div>'; }?>
    </div>
  </div><!-- /.oe-basic -->
</div>
