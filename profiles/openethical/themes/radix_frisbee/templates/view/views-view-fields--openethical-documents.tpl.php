<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */


$sidebar = '';
$class = '';

if(empty($fields['field_featured_image']->content)){
    $class = 'class="no-sidebar"';
}
else{
  $sidebar = '<div class="sidebar">' . $fields['field_featured_image']->content . '</div>';
}

?>

<div class="node-list">
  <div class="oe-basic clearfix <?php if (!empty($class)) { print $class; } ?>">
    <?php print $sidebar ?>
    <div class="contentmain">
      <?php if(!empty($fields['filename']->content)){ print '<h3><a href="">' . ethical_media_get_title_from_filename($fields['filename']->content) . '</a></h3>'; }?>
      
      <?php if(!empty($fields['field_file_author']->content) || !empty($fields['field_file_published_date']->content)){ ?>
      <div class="meta">
        <?php if(!empty($fields['field_file_author']->content)){ print $fields['field_file_author']->content; }?>
        <?php if(!empty($fields['field_file_published_date']->content)){ print $fields['field_file_published_date']->content; }?>
      </div>
      <?php } ?>
      
      <?php if(!empty($fields['field_file_description']->content)){ print $fields['field_file_description']->content; }?>
      <?php if(!empty($fields['rendered']->content)){ print $fields['rendered']->content; }?>
    </div>   
  </div><!-- /.oe-basic -->
</div>
