<!-- STRIPED SECTION CONTENT WITH PROMO -->

<div class="panel-display stripe-content-with-promo clearfix">
  <?php if(!empty($content['field_oe_promo_title']) || !empty($content['field_oe_promo_content']) || !empty($content['field_oe_promo_image'])){ ?>
  <div class="stripe-promo col-md-4">
    <?php print render($content['field_oe_promo_title']); ?>
    <?php print render($content['field_oe_promo_content']); ?>
    <?php print render($content['field_oe_promo_image']); ?>
  </div>
  <?php } ?>
  <?php if(!empty($content['field_oe_section_content'])){ ?>
  <div class="stripe-content col-md-8">
    <?php print render($content['field_oe_section_content']); ?>
    <?php if(!empty($content['field_cta_link'])){ ?>
      <?php print render($content['field_cta_link']); ?>
    <?php } ?>
  </div>
  <?php } ?>
</div>    <!-- end .stripe-content-with-promo -->
