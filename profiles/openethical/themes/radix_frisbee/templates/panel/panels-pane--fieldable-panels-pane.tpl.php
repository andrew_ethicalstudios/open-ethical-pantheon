<?php
/**
 * @file panels-pane.tpl.php
 * Main panel pane template
 *
 * Variables available:
 * - $pane->type: the content type inside this pane
 * - $pane->subtype: The subtype, if applicable. If a view it will be the
 *   view name; if a node it will be the nid, etc.
 * - $title: The title of the content
 * - $content: The actual content
 * - $links: Any links associated with the content
 * - $more: An optional 'more' link (destination only)
 * - $admin_links: Administrative links associated with the content
 * - $feeds: Any feed icons or associated with the content
 * - $display: The complete panels display object containing all kinds of
 *   data including the contexts and all of the other panes being displayed.
 */

$classes2 = '';

$title_field = 'title_field';

if(module_exists('title')){
	$title_field = 'title';
}

if(!empty($variables['content']['field_basic_spotlight_items'])){
  $classes = 'wide-panel';
  $classes2 = ' field-name-field-basic-spotlight-items';
}
?>
<?php if ($pane_prefix): ?>
  <?php print $pane_prefix; ?>
<?php endif; ?>
<div class="<?php print $classes; ?>" <?php print $id; ?>>
  <?php if ($admin_links): ?>
    <?php print $admin_links; ?>
  <?php endif; ?>

  <?php print render($title_prefix); ?>
  <?php if ($title){ ?>
    <h2<?php print $title_attributes; ?>><?php print $title; ?></h2>
  <?php } elseif(!empty($content[$title_field])){ ?>
    <?php if($content[$title_field]['#theme'] == 'html_tag'){ ?>
      <?php if(!empty($content[$title_field]['#value'])){ ?>
        <?php print render($content[$title_field]); ?>
      <?php } ?>
    <?php } else { ?>
      <h2<?php print $title_attributes; ?>><?php print render($content[$title_field]); ?></h2>
    <?php } ?>
  <?php } ?>
  <?php print render($title_suffix); ?>

  <?php if ($feeds): ?>
    <div class="feed">
      <?php print $feeds; ?>
    </div>
  <?php endif; ?>

    <div class="pane-content<?php print $classes2; ?>">
    <?php

      if(!empty($content[$title_field])){
        unset($content[$title_field]);
      }

      print render($content);
    ?>
  </div>

  <?php if ($links): ?>
    <div class="links">
      <?php print $links; ?>
    </div>
  <?php endif; ?>

  <?php if ($more): ?>
    <div class="more-link">
      <?php print $more; ?>
    </div>
  <?php endif; ?>
</div>
<?php if ($pane_suffix): ?>
  <?php print $pane_suffix; ?>
<?php endif; ?>
