(function ($) {
  Drupal.behaviors.ThemeRadixFrisbee = {
    menusCombined: true,
    pageMenuMoved: false,
    searchBoxSidebar: false,
    panelizerGuiActive: false,
    hasViewFilters: false,
    hasCarousel: false,
    attach: function (context, settings) {

      if($('#content div.container-fluid div.radix-layouts-content div.view-filters').length){
        this.hasViewFilters = true;
      }
      if($('#content .panels-ipe-processed').length){
        this.panelizerGuiActive = true;
      }
      this.panelpanes = $('#content div.panel-pane');

      this.combineMenus();
      this.addToSearchBox();
      this.initSubmitSearchForm();
      this.clearBodyText();
      this.visuallySeparatePs();
      this.sortAuthorsWithDates();
      this.floatAdjacentPieceOfContentTeasers();
      this.sortAdjacentTeasers();
      this.indicateLastTags();
      this.closeGaps();
      this.movePageMenu(); //this is an option to move the page menu from the footer to the top of the sidebar for larger screens. To use uncomment or call from client js. Alos uncomment the line a few lines below in the resize bit.
      this.grabSearchBox();

      $(window).resize(function () {
        Drupal.behaviors.ThemeRadixFrisbee.moveCarouselArrows();
        Drupal.behaviors.ThemeRadixFrisbee.combineMenus();
        Drupal.behaviors.ThemeRadixFrisbee.movePageMenu();
        Drupal.behaviors.ThemeRadixFrisbee.grabSearchBox();
        Drupal.behaviors.ThemeRadixFrisbee.sortAdjacentTeasers();
      });

      $(document).ready(function() {
        var carousel = $('#content div.carousel');
        if(carousel.length){
          Drupal.behaviors.ThemeRadixFrisbee.hasCarousel = true;
          Drupal.behaviors.ThemeRadixFrisbee.moveCarouselArrows();
        }

        Drupal.behaviors.ThemeRadixFrisbee.breakFileNamesAtUnderscore();
      });
    },
    breakFileNamesAtUnderscore: function(){
      var arr = $('#content span.file a');
      for(var i = 0; i < arr.length; i++) {
        arr[i].innerHTML = arr[i].innerHTML.replace(/_/g, '_<wbr/>');
      }
    },
    //move carousel arrows depending on image size
    moveCarouselArrows: function(){
      if(this.hasCarousel){
        $('#content div.carousel').each(function(){
          var imgWidth = $(this).width();
          var imgHeight = imgWidth*0.56239316239;
          $(this).find('.carousel-control').css('top', imgHeight/2);
        });
      }
    },
    //add stuff to the search box that appears on faceted search pages (provided by views)
    addToSearchBox: function (){
      if(this.hasViewFilters){
        $('#content div.view-filters')
          .addClass('es-facet-search')
          .attr("role", "search")
          .prepend('<h2 class="search-head">' + Drupal.t('Filter') + '</h2>')
          .append('<p class="more-options hidden-md hidden-lg"><a href="#es-filter">' + Drupal.t('More filter options') + '</a></p>')

        $('#edit-search-api-views-fulltext').attr('placeholder', 'Filter by text');
      }
    },
    //Place the faceted search 'search box' in different places depending on screen width
    grabSearchBox: function (){
       if(this.hasViewFilters){
        var viewFilters = $('#content div.view-filters').first();
        var navBarDisplay = $('#header').find('button.navbar-toggle').css('display');

        if(this.searchBoxSidebar && (navBarDisplay == 'block' || navBarDisplay == 'inline-block')){
          if($('#es-filter').length){
            viewFilters.prepend('<h2 class="search-head">' + Drupal.t('Filter') + '</h2>');
          }
          $('#content div.radix-layouts-content div ul.view').first().prepend(viewFilters);
          this.searchBoxSidebar = false;
        }
        else if(!this.searchBoxSidebar && navBarDisplay == 'none'){
          if($('#es-filter').length){
            $('h2.search-head').remove();
            $('#es-filter').after(viewFilters);
          }
          else{
            $('#content div.row > div.radix-layouts-sidebar div').first().prepend(jQuery('.view-filters'));
          }
          this.searchBoxSidebar = true;
        }
      }
    },
    //Faceted search submit form stuff
    initSubmitSearchForm: function(){
      $('#content div.es-facet-search input.form-submit').click(function(){
        //build the query string (with out excluding anything) for the desired search and load the page
        window.location = Drupal.behaviors.EthicalFacetedFacetApiMultiselectWidget.buildQueryString('');
        return false;
      });
    },
    //clear as in for floats. Necessary because some trickery is done to float featured image next to field-body. But if there is no image, all hell breaks loose
    clearBodyText: function(){

      var fieldBody = $('#content div.field-body').first();

      //if panelizer gui active
      if(this.panelizerGuiActive){
        if(fieldBody.closest('div.panels-ipe-portlet-wrapper').prev().find('div.field-featured-image').length == 0){
          if(!fieldBody.prev().is('.field-featured-image')){
            fieldBody.addClass('field-body-no-image');
          }
        }
      }
      else{
        if(!fieldBody.prev().is('.field-featured-image')){
          fieldBody.addClass('field-body-no-image');
        }
      }
    },
    //if the first panel after some unwrapped p then some special styling is needed
    visuallySeparatePs: function(){

      $.each(this.panelpanes, function(i, val){
        if($(val)[0].previousElementSibling && $(val)[0].previousElementSibling.tagName == 'P'){
          $(this).addClass('first-panel');
        }
      });

      //above won't work if panelizer gui active so add first panel in a slightly different way
      if(this.panelizerGuiActive){
        $.each($('#content .panels-ipe-portlet-wrapper'), function(i, val){

          if($(val).find('div.panel-pane').length && !$(val).find('div.easy_social_box').length){

            if($(val)[0].previousElementSibling){

              var elm = $(val)[0].previousElementSibling;

              if($(elm).hasClass('panels-ipe-portlet-wrapper')){
                if(!$(elm).find('div.panel-pane').length){
                  $(this).addClass('first-panel');
                }
              }
            }
          }
        });
      }
    },
    //Some items have authors and dates. Others have one or the other.
    //Add some classes to help with styling the different scenarios
    sortAuthorsWithDates: function(){
      $.each($('#content ul.oe-portrait-search-result p.date, #content ul.oe-portrait-fields p.date'), function(i, val){
        if($(val).prev().hasClass('author')){
          $(val).parent().addClass('hasAuthorAndDate');
        }
      });
    },
    //teasers in wider columns are floated next to each other.
    //The teasers at the bottom of the list should not have separators at the bottom.
    //But how to determine which, and how many, are at the bottom if floating is going on?
    //Adding some classes to help
    sortAdjacentTeasers: function(){
      $.each($('#content ul.oe-teaser, #content ul.oe-portrait-teaser'), function(i, val){
        var ul = $(val);
        var numLi = ul.children().length;
        var panelpanel = $(this).closest('div.panel-panel');

        //if it's a wide panel (and isn't a nested col-md- (eg a col-md-12 inside a col-md-4) [which would make it narrow])
        //then need third-last as well to deal with possibility of 3 teasers being next to each other in a row
        //doesn't happen very often
        if(panelpanel.hasClass('col-md-12') && panelpanel.parent().parent().hasClass('container')){
          $(this).children("li:nth-child(3n)").addClass("views-row-third");

          if(numLi % 3 == 0){
            var ulWidth = ul.width();
            var secondToLast = ul.children('li:nth-child(' + (numLi-1) + ')');
            var thirdToLast = ul.children('li:nth-child(' + (numLi-2) + ')');
            var liWidth = secondToLast.width();

            if(ulWidth > 3*liWidth){
              secondToLast.addClass('second-last');
              thirdToLast.addClass('third-last');
            }
            else{
              secondToLast.removeClass('second-last');
              thirdToLast.removeClass('third-last');
            }
          }
          else if(numLi % 3 == 2){
            var ulWidth = ul.width();
            var secondToLast = ul.children('li:nth-child(' + (numLi-1) + ')');
            var liWidth = secondToLast.width();

            if(ulWidth > 3*liWidth){
              secondToLast.addClass('second-last');
            }
            else{
              secondToLast.removeClass('second-last');
            }
          }
        }
        //normally there will only every be 2 teasers in a row
        else{
          if(numLi % 2 == 0){
            var ulWidth = ul.width();
            var secondToLast = ul.children('li:nth-child(' + (numLi-1) + ')');
            var liWidth = secondToLast.width();

            if(ulWidth > 2*liWidth){
              secondToLast.addClass('second-last');
            }
            else{
              secondToLast.removeClass('second-last');
            }
          }
        }
      });
    },
    //Teasers can be added one at a time (instead of as a list) using for example 'add content item' or 'add custom item'
    //If these teasers are next to each other they should be presented in one or more rows (as per teaser lists)
    //To do this a ul is wrapped around the teaser panels
    floatAdjacentPieceOfContentTeasers: function(){

      if(!$('#content div.pieceIsTeaser').length){

        //identify the teasers we want to wrap
        //if the ipe is not active
        if(!this.panelizerGuiActive){
          $('#content div.pane-panopoly-widgets-general-content-piece-of-content').find('.oe-teaser').each(function(){
            $(this).closest('div.pane-panopoly-widgets-general-content-piece-of-content').addClass('pieceIsTeaser');
          })

          $('#content div.pane-bundle-custom-item').find('.oe-teaser').each(function(){
            $(this).closest('div.pane-bundle-custom-item').addClass('pieceIsTeaser');
          })

          $('#content div.pane-openethical-documents-panel-pane-1').find('.oe-portrait-teaser').each(function(){
            $(this).closest('div.pane-openethical-documents-panel-pane-1').addClass('pieceIsTeaser');
          })
        }
        //if the ipe is present (because logged in) and giving extra divs
        else{
          $('#content div.pane-panopoly-widgets-general-content-piece-of-content .oe-teaser, #content div.pane-bundle-custom-item .oe-teaser, #content div.pane-openethical-documents-panel-pane-1 .oe-portrait-teaser').each(function(){
            $(this).closest('div.panels-ipe-portlet-wrapper').addClass('pieceIsTeaser');
          })
        }

        var teaserPieces = $('#content div.pieceIsTeaser');

        //if teaser has a section (h2)
        teaserPieces.each(function(i){
            if($(this).find('h2:first').length > 0){
              $(this).addClass('hasSectionHeader');
            }
            else{
              $(this).addClass('noSectionHeader');
            }
        });

        //where groups of teasers are present (ie teasers next to each other)
        //h2's are supposed to be the header for the group
        //h3's are supposed to be the header for the teaser
        //so if a teaser has an h2 am assuming it's the first of a group of teasers to be wrapped
        //Also need to move the h2 up out of the individual teaser and onto the group
        $('#content div.hasSectionHeader')
        .map(function() {
          return $(this).nextUntil(":not(.noSectionHeader)").andSelf();
        })
        .wrap('<div class="teaserGroup" />');

        $('#content div.teaserGroup h2').each(function(i){
          $(this).prependTo($(this).closest('div.teaserGroup'));
        });

        //http://stackoverflow.com/questions/11242430/can-i-wrap-around-a-collection-of-elements-in-an-array
        teaserPieces
        .filter(function() {
          return !$(this).prev().is(".pieceIsTeaser");
        })
        .map(function() {
          return $(this).nextUntil(":not(.pieceIsTeaser)").andSelf();
        })
        .wrap('<ul class="teaserPieces oe-teaser" />');

        $('#content ul.teaserPieces').each(function(i){
          $(this).children('div.pieceIsTeaser').wrap('<li />');
          $(this).children('li').filter(':even').addClass('pieces-odd');
          //the first panel after a non-panel wrapped p gets special styling.
          //However we've wrapped up some panels, so make sure if there was a 'first-panel' that class is moved up to the wrapper
          $(this).find('div.first-panel').removeClass('first-panel').closest('ul.teaserPieces').addClass('first-panel');
        });
      }
    },
    indicateLastTags: function(){
      $.each(this.panelpanes, function(i, val){
        $('div.tags').last().addClass('last-tags').parent().removeClass('es-bear-bones');
      });
    },
    closeGaps: function(){
      $.each($('#content div.oe-basic span.file'), function(i, val){
        if($(val)[0].previousElementSibling && $(val)[0].previousElementSibling.tagName == 'P'){
          $(this).addClass('shift-up');
        }
      });
    },
    //combine the main-menu and secondary menu if the burger is showing (ie it's a small screen size)
    combineMenus: function(){
      if($('#secondary-menu').length){
        var navBarDisplay = $('#header').find('button.navbar-toggle').css('display');
        if(!this.menusCombined && (navBarDisplay == 'block' || navBarDisplay == 'inline-block')){
          $('#header-items').insertAfter('#main-menu').prepend($('#secondary-menu')).removeClass('pull-right');
          this.menusCombined = true;
        }
        else if(this.menusCombined && navBarDisplay == 'none'){
          $('#header-items').insertAfter('#navbar-header').append($('#secondary-menu')).addClass('pull-right');
          this.menusCombined = false;
        }
      }
    },
    //move the page menu from the footer into the sidebar
    movePageMenu: function(){

      var footer = $('#main-wrapper div.panel-panel.radix-layouts-footer').first();

      if($('#main-wrapper div.panel-panel.radix-layouts-sidebar').length){
        var sidebar = $('#main-wrapper div.panel-panel.radix-layouts-sidebar').first();
      }
      else{
        var sidebar = $('#main-wrapper div.panel-panel.radix-layouts-slidergutter').first();
      }

      if(footer.length && sidebar.length){
        var navBarDisplay = $('#header').find('button.navbar-toggle').css('display');

        if(!this.pageMenuMoved && navBarDisplay == 'none'){

          var pageMenu = footer.find('div.pane-main-menu');

          if(this.panelizerGuiActive){
            var wrapper = pageMenu.closest('.panels-ipe-portlet-wrapper');
          }

          pageMenu.prependTo(sidebar);

          if(this.panelizerGuiActive){
            //hide the panelizer gui wrapper for the menu (the one with a drag handle on it).
            //editors can't use it anyway and it looks weird being empty (it's empty because the above js has moved the menu out of it)
            wrapper.hide();
          }

          this.pageMenuMoved = true;
        }
        else if(this.pageMenuMoved && (navBarDisplay == 'block' || navBarDisplay == 'inline-block')){
          var pageMenu = sidebar.find('div.pane-main-menu');
          pageMenu.prependTo(footer);
          this.pageMenuMoved = false;
        }
      }
    }
  }
})(jQuery);
