<?php
// Plugin definition
$plugin = array(
  'title' => t('OE Mountain'),
  'icon' => 'oe-mountain.png',
  'category' => t('Open Ethical'),
  'theme' => 'oe_mountain',
  'regions' => array(
    'slider' => t('Slider'),
    'slidergutter' => t('Slider Gutter'),
    'contentmain' => t('Content'),
    'footer' => t('Footer'),
  ),
);
