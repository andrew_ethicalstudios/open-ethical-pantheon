<?php
/**
 * @file
 * Template for OE Basic.
 *
 * Used widely across the site for teasers and featured items.
 * Has some elaborate CSS to make it look good across lots of screen sizes.
 *
 * Variables:
 * - $css_id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 * panel of the layout. This layout supports the following sections:
 */

$sidebar = '';
$header = '';

if(empty($content['sidebar'])){
  if (!empty($class)) {
    $class .= ' no-sidebar';
  }
  else {
    $class = 'no-sidebar';
  }
}

if(empty($content['header'])){
  if (!empty($class)) {
    $class .= ' no-header';
  }
  else {
    $class = 'no-header';
  }
}

?>

<div class="panel-display oe-basic clearfix <?php if (!empty($class)) { print $class; } ?>" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
  <?php if(!empty($content['header'])){ ?>
    <div class="header"><?php print $content['header']; ?></div>
  <?php } ?>
  <?php if(!empty($content['sidebar'])){ ?>
    <div class="sidebar"><?php print $content['sidebar']; ?></div>
  <?php } ?>
  <div class="contentmain">
    <?php print $content['contentmain']; ?>
  </div>
</div><!-- /.oe-basic -->
