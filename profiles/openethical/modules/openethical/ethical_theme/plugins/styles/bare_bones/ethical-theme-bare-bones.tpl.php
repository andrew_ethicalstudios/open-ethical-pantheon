<?php
// Used to provide a panel style that doesn't have any bottom vertical margin, border or padding
// Useful for when you want to make two panels (one after the other) look like they are one panel

$title = '';

//multi langugage
if(module_exists('title') && isset($content->content['title_field'])){
  $title_field = $content->content['title_field'];
  $title = render($title_field);
}
elseif(isset($content->title)){
  $title = $content->title;
}

$type_class = 'pane-' . strtr($variables['content']->type, array('_' => '-'));
$subtype_class = 'pane-' . strtr($variables['content']->subtype, array('_' => '-'));

?>

<div class="panel-pane <?php print $type_class . ' ' . $subtype_class; ?> es-bear-bones clearfix">
  <?php if (!empty($title)): ?>
  <h2 class="pane-title"><?php print $title; ?></h2>
  <?php endif ?>
  <?php
    if(gettype($content->content) != 'string'){
      if(module_exists('title') && isset($content->content['title_field'])){
        unset($content->content['title_field']);
      } else if(isset($content->content['title'])) {
        unset($content->content['title']);
      }
    }
    ?>
  <?php print render($content->content); ?>
</div>
