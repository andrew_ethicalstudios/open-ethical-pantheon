<?php

/**
* Plugin definition
* Used to provide a panel style that doesn't have any bottom vertical margin, border or padding
* Useful for when you want to make two panels (one after the other) look like they are one panel
*/
$plugin =  array(
  'title' => t('Bare bones'),
  'description'   => t('A stripped down style'),
  'render pane' => 'ethical_theme_render_bare_bones_pane',
  'hook theme'    => array(
    'ethical_theme_bare_bones' => array(
      'template' => 'ethical-theme-bare-bones',
      'path' => drupal_get_path('module', 'ethical_theme') .'/plugins/styles/bare_bones',
      'variables' => array(
        'content' => NULL,
        'settings' => NULL,
      ),
    ),
  ),
);

/**
 * Render callback
 */
function theme_ethical_theme_render_bare_bones_pane($vars) {
    $settings = $vars['settings'];
    $content = $vars['content'];
  return theme('ethical_theme_bare_bones', array('content' => $content, 'settings' => $settings));
}
