<?php
/**
 * @file
 * ethical_theme.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function ethical_theme_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: filter
  $overrides["filter.panopoly_html_text.filters|filter_url|weight"] = -43;
  $overrides["filter.panopoly_html_text.filters|spamspan"] = array(
    'settings' => array(
      'spamspan_at' => ' [at] ',
      'spamspan_dot' => ' [dot] ',
      'spamspan_dot_enable' => 0,
      'spamspan_email_default' => 'contact_us_general_enquiry',
      'spamspan_email_encode' => TRUE,
      'spamspan_form_default_displaytext' => 'contact form',
      'spamspan_form_default_url' => 'contact',
      'spamspan_form_pattern' => '<a href="%url?goto=%email">%displaytext</a>',
      'spamspan_use_form' => FALSE,
      'spamspan_use_graphic' => 0,
      'spamspan_use_url' => '<a href="/#formname?goto=#email">#displaytext</a>',
    ),
    'status' => 1,
    'weight' => -44,
  );
  $overrides["filter.panopoly_wysiwyg_text.filters|caption_filter"]["DELETED"] = TRUE;
  $overrides["filter.panopoly_wysiwyg_text.filters|filter_autop"] = array(
    'settings' => array(),
    'status' => 1,
    'weight' => -45,
  );
  $overrides["filter.panopoly_wysiwyg_text.filters|panopoly_images_fix_captions"]["DELETED"] = TRUE;
  $overrides["filter.panopoly_wysiwyg_text.filters|spamspan|settings|spamspan_dot"] = ' [dot] ';
  $overrides["filter.panopoly_wysiwyg_text.filters|spamspan|settings|spamspan_dot_enable"] = 0;
  $overrides["filter.panopoly_wysiwyg_text.filters|spamspan|settings|spamspan_email_default"] = 'contact_us_general_enquiry';
  $overrides["filter.panopoly_wysiwyg_text.filters|spamspan|settings|spamspan_email_encode"] = TRUE;
  $overrides["filter.panopoly_wysiwyg_text.filters|spamspan|settings|spamspan_form_default_displaytext"] = 'contact form';
  $overrides["filter.panopoly_wysiwyg_text.filters|spamspan|settings|spamspan_form_default_url"] = 'contact';
  $overrides["filter.panopoly_wysiwyg_text.filters|spamspan|settings|spamspan_form_pattern"] = '<a href="%url?goto=%email">%displaytext</a>';
  $overrides["filter.panopoly_wysiwyg_text.filters|spamspan|settings|spamspan_use_form"] = FALSE;
  $overrides["filter.panopoly_wysiwyg_text.filters|spamspan|settings|spamspan_use_url"] = '<a href="/#formname?goto=#email">#displaytext</a>';
  $overrides["filter.panopoly_wysiwyg_text.filters|spamspan|settings|use_form"] = array(
    'spamspan_email_default' => 'contact_us_general_enquiry',
    'spamspan_email_encode' => 1,
    'spamspan_use_form' => 0,
    'spamspan_use_url' => '<a href="/#formname?goto=#email">#displaytext</a>',
  );

  // Exported overrides for: views_view
  $overrides["views_view.panopoly_database_search.display|default|display_options|css_class"] = 'oe-featured';
  $overrides["views_view.panopoly_database_search.display|default|display_options|row_options"] = array(
    'view_mode' => 'featured',
  );
  $overrides["views_view.panopoly_database_search.display|default|display_options|row_plugin"] = 'entity';

  // Exported overrides for: wysiwyg
  $overrides["wysiwyg.panopoly_wysiwyg_text.settings|buttons|captionfilter"]["DELETED"] = TRUE;

 return $overrides;
}
