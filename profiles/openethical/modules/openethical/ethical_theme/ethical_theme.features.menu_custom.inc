<?php
/**
 * @file
 * ethical_theme.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function ethical_theme_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-secondary-menu.
  $menus['menu-secondary-menu'] = array(
    'menu_name' => 'menu-secondary-menu',
    'title' => 'Secondary menu',
    'description' => '',
  );
  // Exported menu: menu-tertiary-menu.
  $menus['menu-tertiary-menu'] = array(
    'menu_name' => 'menu-tertiary-menu',
    'title' => 'Tertiary menu',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Secondary menu');
  t('Tertiary menu');


  return $menus;
}
