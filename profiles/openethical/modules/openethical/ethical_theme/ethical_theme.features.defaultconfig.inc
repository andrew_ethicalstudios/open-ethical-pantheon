<?php
/**
 * @file
 * ethical_theme.features.defaultconfig.inc
 */

/**
 * Implements hook_defaultconfig_features().
 */
function ethical_theme_defaultconfig_features() {
  return array(
    'ethical_theme' => array(
      'strongarm' => 'strongarm',
      'user_default_permissions' => 'user_default_permissions',
    ),
  );
}

/**
 * Implements hook_defaultconfig_strongarm().
 */
function ethical_theme_defaultconfig_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'fontyourface_css_md5';
  $strongarm->value = 'd41d8cd98f00b204e9800998ecf8427e';
  $export['fontyourface_css_md5'] = $strongarm;

  return $export;
}

/**
 * Implements hook_defaultconfig_user_default_permissions().
 */
function ethical_theme_defaultconfig_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'oe_administer_ethical_theme'.
  $permissions['oe_administer_ethical_theme'] = array(
    'name' => 'oe_administer_ethical_theme',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'ethical_theme',
  );

  return $permissions;
}
