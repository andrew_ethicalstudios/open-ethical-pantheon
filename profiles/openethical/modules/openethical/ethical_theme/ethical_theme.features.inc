<?php
/**
 * @file
 * ethical_theme.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ethical_theme_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_filter_default_formats_alter().
 */
function ethical_theme_filter_default_formats_alter(&$data) {
  if (isset($data['panopoly_html_text'])) {
    $data['panopoly_html_text']['filters']['filter_url']['weight'] = -43; /* WAS: -44 */
    $data['panopoly_html_text']['filters']['spamspan'] = array(
      'settings' => array(
        'spamspan_at' => ' [at] ',
        'spamspan_dot' => ' [dot] ',
        'spamspan_dot_enable' => 0,
        'spamspan_email_default' => 'contact_us_general_enquiry',
        'spamspan_email_encode' => TRUE,
        'spamspan_form_default_displaytext' => 'contact form',
        'spamspan_form_default_url' => 'contact',
        'spamspan_form_pattern' => '<a href="%url?goto=%email">%displaytext</a>',
        'spamspan_use_form' => FALSE,
        'spamspan_use_graphic' => 0,
        'spamspan_use_url' => '<a href="/#formname?goto=#email">#displaytext</a>',
      ),
      'status' => 1,
      'weight' => -44,
    ); /* WAS: '' */
  }
  if (isset($data['panopoly_wysiwyg_text'])) {
    $data['panopoly_wysiwyg_text']['filters']['filter_autop'] = array(
      'settings' => array(),
      'status' => 1,
      'weight' => -45,
    ); /* WAS: '' */
    $data['panopoly_wysiwyg_text']['filters']['spamspan']['settings']['spamspan_dot'] = ' [dot] '; /* WAS: '' */
    $data['panopoly_wysiwyg_text']['filters']['spamspan']['settings']['spamspan_dot_enable'] = 0; /* WAS: '' */
    $data['panopoly_wysiwyg_text']['filters']['spamspan']['settings']['spamspan_email_default'] = 'contact_us_general_enquiry'; /* WAS: '' */
    $data['panopoly_wysiwyg_text']['filters']['spamspan']['settings']['spamspan_email_encode'] = TRUE; /* WAS: '' */
    $data['panopoly_wysiwyg_text']['filters']['spamspan']['settings']['spamspan_form_default_displaytext'] = 'contact form'; /* WAS: '' */
    $data['panopoly_wysiwyg_text']['filters']['spamspan']['settings']['spamspan_form_default_url'] = 'contact'; /* WAS: '' */
    $data['panopoly_wysiwyg_text']['filters']['spamspan']['settings']['spamspan_form_pattern'] = '<a href="%url?goto=%email">%displaytext</a>'; /* WAS: '' */
    $data['panopoly_wysiwyg_text']['filters']['spamspan']['settings']['spamspan_use_form'] = FALSE; /* WAS: '' */
    $data['panopoly_wysiwyg_text']['filters']['spamspan']['settings']['spamspan_use_url'] = '<a href="/#formname?goto=#email">#displaytext</a>'; /* WAS: '' */
    $data['panopoly_wysiwyg_text']['filters']['spamspan']['settings']['use_form'] = array(
      'spamspan_email_default' => 'contact_us_general_enquiry',
      'spamspan_email_encode' => 1,
      'spamspan_use_form' => 0,
      'spamspan_use_url' => '<a href="/#formname?goto=#email">#displaytext</a>',
    ); /* WAS: '' */
    unset($data['panopoly_wysiwyg_text']['filters']['caption_filter']);
    unset($data['panopoly_wysiwyg_text']['filters']['panopoly_images_fix_captions']);
  }
}

/**
 * Implements hook_views_default_views_alter().
 */
function ethical_theme_views_default_views_alter(&$data) {
  if (isset($data['panopoly_database_search'])) {
    $data['panopoly_database_search']->display['default']->display_options['css_class'] = 'oe-featured'; /* WAS: '' */
    $data['panopoly_database_search']->display['default']->display_options['row_options'] = array(
      'view_mode' => 'featured',
    ); /* WAS: '' */
    $data['panopoly_database_search']->display['default']->display_options['row_plugin'] = 'entity'; /* WAS: 'fields' */
  }
}

/**
 * Implements hook_wysiwyg_default_profiles_alter().
 */
function ethical_theme_wysiwyg_default_profiles_alter(&$data) {
  if (isset($data['panopoly_wysiwyg_text'])) {
    unset($data['panopoly_wysiwyg_text']['settings']['buttons']['captionfilter']);
  }
}
