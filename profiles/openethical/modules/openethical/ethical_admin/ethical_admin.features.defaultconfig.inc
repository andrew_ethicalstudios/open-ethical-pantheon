<?php
/**
 * @file
 * ethical_admin.features.defaultconfig.inc
 */

/**
 * Implements hook_defaultconfig_features().
 */
function ethical_admin_defaultconfig_features() {
  return array(
    'ethical_admin' => array(
      'strongarm' => 'strongarm',
      'user_default_permissions' => 'user_default_permissions',
    ),
  );
}

/**
 * Implements hook_defaultconfig_strongarm().
 */
function ethical_admin_defaultconfig_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'admin_theme';
  $strongarm->value = 'seven';
  $export['admin_theme'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_admin_theme';
  $strongarm->value = 1;
  $export['node_admin_theme'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'panopoly_core_breadcrumb_title';
  $strongarm->value = '0';
  $export['panopoly_core_breadcrumb_title'] = $strongarm;

  return $export;
}

/**
 * Implements hook_defaultconfig_user_default_permissions().
 */
function ethical_admin_defaultconfig_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'oe_edit_ipe_fields'.
  $permissions['oe_edit_ipe_fields'] = array(
    'name' => 'oe_edit_ipe_fields',
    'roles' => array(),
    'module' => 'ethical_admin',
  );

  // Exported permission: 'oe_view_ipe_category_admin'.
  $permissions['oe_view_ipe_category_admin'] = array(
    'name' => 'oe_view_ipe_category_admin',
    'roles' => array(),
    'module' => 'ethical_admin',
  );

  // Exported permission: 'oe_view_ipe_category_blocks'.
  $permissions['oe_view_ipe_category_blocks'] = array(
    'name' => 'oe_view_ipe_category_blocks',
    'roles' => array(),
    'module' => 'ethical_admin',
  );

  // Exported permission: 'oe_view_ipe_category_display_suite'.
  $permissions['oe_view_ipe_category_display_suite'] = array(
    'name' => 'oe_view_ipe_category_display_suite',
    'roles' => array(),
    'module' => 'ethical_admin',
  );

  // Exported permission: 'oe_view_ipe_category_form_content'.
  $permissions['oe_view_ipe_category_form_content'] = array(
    'name' => 'oe_view_ipe_category_form_content',
    'roles' => array(),
    'module' => 'ethical_admin',
  );

  // Exported permission: 'oe_view_ipe_category_menus'.
  $permissions['oe_view_ipe_category_menus'] = array(
    'name' => 'oe_view_ipe_category_menus',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'ethical_admin',
  );

  // Exported permission: 'oe_view_ipe_category_page_content'.
  $permissions['oe_view_ipe_category_page_content'] = array(
    'name' => 'oe_view_ipe_category_page_content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'ethical_admin',
  );

  return $permissions;
}
