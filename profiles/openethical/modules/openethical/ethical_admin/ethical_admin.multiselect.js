(function ($) {
  
  Drupal.behaviors.EthicalAdminMultiselectWidget = {
    attach: function (context, settings) {

      if($(".field-type-taxonomy-term-reference select").length > 0){
        $(".field-type-taxonomy-term-reference select").chosen({width: "100%", placeholder_text_multiple: "Select", placeholder_text_single: "Select"});
      }
      if($(".field-type-entityreference select").length > 0){
        $(".field-type-entityreference select").chosen({width: "100%", placeholder_text_multiple: "Select", placeholder_text_single: "Select"});
      }
      if($(".field-type-country select").length > 0){
        $(".field-type-country select").chosen({width: "100%", placeholder_text_multiple: "Select", placeholder_text_single: "Select"});
      }
      if($(".field-type-list-integer select").length > 0){
        $(".field-type-list-integer select").chosen({width: "100%", placeholder_text_multiple: "Select", placeholder_text_single: "Select"});
      }
      
      if($("#views-exposed-pane select").length > 0){
        $("#views-exposed-pane select").chosen({width: "100%", placeholder_text_multiple: "Select", placeholder_text_single: "Select"});
      }
    }
  };
 
})(jQuery);

