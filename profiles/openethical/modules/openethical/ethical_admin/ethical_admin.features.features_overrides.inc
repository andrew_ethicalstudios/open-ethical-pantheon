<?php
/**
 * @file
 * ethical_admin.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function ethical_admin_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: page_manager_handlers
  $overrides["page_manager_handlers.node_edit_panel_context.conf|display|content|new-51126c95-cf51-4ad6-a27a-11e4260d8023|configuration|override_title_text"] = 'Category';
  $overrides["page_manager_handlers.node_edit_panel_context.conf|display|content|new-51126c95-cf51-4ad6-a27a-11e4260d8023|panel"] = 'contentmain';
  $overrides["page_manager_handlers.node_edit_panel_context.conf|display|content|new-51126c95-cf51-4ad6-a27a-11e4260d8023|position"] = 4;
  $overrides["page_manager_handlers.node_edit_panel_context.conf|display|content|new-6e4a98d8-3738-41b9-8c33-4b4c905c6834|position"] = 3;
  $overrides["page_manager_handlers.node_edit_panel_context.conf|display|content|new-9176b962-e64f-44a6-8ebe-e03ebf0f6c3d"]["DELETED"] = TRUE;
  $overrides["page_manager_handlers.node_edit_panel_context.conf|display|content|new-99f6c0bd-36fd-411b-9833-c0be0fab7548|position"] = 1;
  $overrides["page_manager_handlers.node_edit_panel_context.conf|display|content|new-acdc066a-b785-4baa-99ad-eb8fe70429fb"] = (object) array(
      'pid' => 'new-acdc066a-b785-4baa-99ad-eb8fe70429fb',
      'panel' => 'contentmain',
      'type' => 'entity_form_field',
      'subtype' => 'node:field_project',
      'shown' => TRUE,
      'access' => array(),
      'configuration' => array(
        'label' => '',
        'formatter' => '',
        'context' => 'argument_node_edit_1',
        'override_title' => 1,
        'override_title_text' => 'Project',
      ),
      'cache' => array(),
      'style' => '',
      'css' => array(),
      'extras' => array(),
      'position' => 3,
      'locks' => array(),
      'uuid' => 'acdc066a-b785-4baa-99ad-eb8fe70429fb',
    );
  $overrides["page_manager_handlers.node_edit_panel_context.conf|display|content|new-c0926cca-059c-4bb1-bccd-2838ec7a93f2|position"] = 4;
  $overrides["page_manager_handlers.node_edit_panel_context.conf|display|content|new-cb6bcf52-b51c-4e7e-a348-84d4081d8273"] = (object) array(
      'pid' => 'new-cb6bcf52-b51c-4e7e-a348-84d4081d8273',
      'panel' => 'contentmain',
      'type' => 'entity_form_field',
      'subtype' => 'node:field_country',
      'shown' => TRUE,
      'access' => array(),
      'configuration' => array(
        'label' => '',
        'formatter' => '',
        'context' => 'argument_node_edit_1',
        'override_title' => 1,
        'override_title_text' => 'Country',
      ),
      'cache' => array(),
      'style' => '',
      'css' => array(),
      'extras' => array(),
      'position' => 5,
      'locks' => array(),
      'uuid' => 'cb6bcf52-b51c-4e7e-a348-84d4081d8273',
    );
  $overrides["page_manager_handlers.node_edit_panel_context.conf|display|content|new-e38ab3a3-24d0-4ac2-900e-4ddb0248be0c|position"] = 2;
  $overrides["page_manager_handlers.node_edit_panel_context.conf|display|content|new-fc76a7e2-ac1b-4ed7-b79b-3ee8c28c38c6"] = (object) array(
      'pid' => 'new-fc76a7e2-ac1b-4ed7-b79b-3ee8c28c38c6',
      'panel' => 'contentmain',
      'type' => 'entity_form_field',
      'subtype' => 'node:field_group',
      'shown' => TRUE,
      'access' => array(),
      'configuration' => array(
        'label' => '',
        'formatter' => '',
        'context' => 'argument_node_edit_1',
        'override_title' => 1,
        'override_title_text' => 'Group',
      ),
      'cache' => array(),
      'style' => '',
      'css' => array(),
      'extras' => array(),
      'position' => 6,
      'locks' => array(),
      'uuid' => 'fc76a7e2-ac1b-4ed7-b79b-3ee8c28c38c6',
    );
  $overrides["page_manager_handlers.node_edit_panel_context.conf|display|layout"] = 'oe-admin';
  $overrides["page_manager_handlers.node_edit_panel_context.conf|display|panels|contentmain|3"] = 'new-51126c95-cf51-4ad6-a27a-11e4260d8023';
  $overrides["page_manager_handlers.node_edit_panel_context.conf|display|panels|contentmain|4"] = 'new-acdc066a-b785-4baa-99ad-eb8fe70429fb';
  $overrides["page_manager_handlers.node_edit_panel_context.conf|display|panels|contentmain|5"] = 'new-cb6bcf52-b51c-4e7e-a348-84d4081d8273';
  $overrides["page_manager_handlers.node_edit_panel_context.conf|display|panels|contentmain|6"] = 'new-fc76a7e2-ac1b-4ed7-b79b-3ee8c28c38c6';
  $overrides["page_manager_handlers.node_edit_panel_context.conf|display|panels|sidebar|1"] = 'new-99f6c0bd-36fd-411b-9833-c0be0fab7548';
  $overrides["page_manager_handlers.node_edit_panel_context.conf|display|panels|sidebar|2"] = 'new-e38ab3a3-24d0-4ac2-900e-4ddb0248be0c';
  $overrides["page_manager_handlers.node_edit_panel_context.conf|display|panels|sidebar|3"] = 'new-6e4a98d8-3738-41b9-8c33-4b4c905c6834';
  $overrides["page_manager_handlers.node_edit_panel_context.conf|display|panels|sidebar|4"] = 'new-c0926cca-059c-4bb1-bccd-2838ec7a93f2';
  $overrides["page_manager_handlers.node_edit_panel_context.conf|display|panels|sidebar|5"]["DELETED"] = TRUE;
  $overrides["page_manager_handlers.node_edit_panel_context.conf|display|panels|sidebar|6"]["DELETED"] = TRUE;
  $overrides["page_manager_handlers.user_edit_panel_context.conf|display|content|new-be0c59f6-016f-4a3d-a888-dd3cf534796f"]["DELETED"] = TRUE;
  $overrides["page_manager_handlers.user_edit_panel_context.conf|display|panel_settings|style_settings|footer"] = NULL;
  $overrides["page_manager_handlers.user_edit_panel_context.conf|display|panels|sidebar"]["DELETED"] = TRUE;

 return $overrides;
}
