<?php
/**
 * @file
 * ethical_widgets.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function ethical_widgets_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create fieldable custom_item'.
  $permissions['create fieldable custom_item'] = array(
    'name' => 'create fieldable custom_item',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'fieldable_panels_panes',
  );

  // Exported permission: 'delete fieldable custom_item'.
  $permissions['delete fieldable custom_item'] = array(
    'name' => 'delete fieldable custom_item',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'fieldable_panels_panes',
  );

  // Exported permission: 'edit fieldable custom_item'.
  $permissions['edit fieldable custom_item'] = array(
    'name' => 'edit fieldable custom_item',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'fieldable_panels_panes',
  );

  return $permissions;
}
