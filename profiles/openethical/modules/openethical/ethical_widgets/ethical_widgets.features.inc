<?php
/**
 * @file
 * ethical_widgets.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ethical_widgets_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "panelizer" && $api == "panelizer") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_field_default_field_instances_alter().
 */
function ethical_widgets_field_default_field_instances_alter(&$data) {
  if (isset($data['fieldable_panels_pane-spotlight-field_basic_spotlight_items'])) {
    $data['fieldable_panels_pane-spotlight-field_basic_spotlight_items']['display']['default']['settings']['image_style'] = 'full'; /* WAS: 'panopoly_image_spotlight' */
    $data['fieldable_panels_pane-spotlight-field_basic_spotlight_items']['fences_wrapper'] = 'no_wrapper'; /* WAS: '' */
  }
}
