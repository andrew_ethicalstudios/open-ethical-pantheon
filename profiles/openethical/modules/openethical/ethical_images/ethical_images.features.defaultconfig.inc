<?php
/**
 * @file
 * ethical_images.features.defaultconfig.inc
 */

/**
 * Implements hook_defaultconfig_features().
 */
function ethical_images_defaultconfig_features() {
  return array(
    'ethical_images' => array(
      'user_default_permissions' => 'user_default_permissions',
    ),
  );
}

/**
 * Implements hook_defaultconfig_user_default_permissions().
 */
function ethical_images_defaultconfig_user_default_permissions() {
  $permissions = array();



  return $permissions;
}
