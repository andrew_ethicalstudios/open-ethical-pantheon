<?php
/**
 * @file
 * ethical_images.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ethical_images_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "file_entity" && $api == "file_default_displays") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function ethical_images_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_file_default_displays_alter().
 */
function ethical_images_file_default_displays_alter(&$data) {
  if (isset($data['image__default__file_field_file_default'])) {
    $data['image__default__file_field_file_default']->status = FALSE; /* WAS: TRUE */
  }
  if (isset($data['image__preview__file_field_file_default'])) {
    $data['image__preview__file_field_file_default']->status = FALSE; /* WAS: TRUE */
  }
  if (isset($data['image__teaser__file_field_file_default'])) {
    $data['image__teaser__file_field_file_default']->status = FALSE; /* WAS: TRUE */
  }
}

/**
 * Implements hook_image_styles_alter().
 */
function ethical_images_image_styles_alter(&$data) {
  if (isset($data['large'])) {

  if (!isset($data['large']['storage']) || $data['large']['storage'] == IMAGE_STORAGE_DEFAULT) {
    $data['large']['effects'][0]['data']['height'] = 420;
  }

  if (!isset($data['large']['storage']) || $data['large']['storage'] == IMAGE_STORAGE_DEFAULT) {
    $data['large']['effects'][0]['data']['onlyscaleifcrop'] = 0;
  }

  if (!isset($data['large']['storage']) || $data['large']['storage'] == IMAGE_STORAGE_DEFAULT) {
    $data['large']['effects'][0]['data']['respectminimum'] = 1;
  }

  if (!isset($data['large']['storage']) || $data['large']['storage'] == IMAGE_STORAGE_DEFAULT) {
    $data['large']['effects'][0]['data']['style_name'] = 'large';
  }

  if (!isset($data['large']['storage']) || $data['large']['storage'] == IMAGE_STORAGE_DEFAULT) {
    $data['large']['effects'][0]['data']['upscale'] = 1;
  }

  if (!isset($data['large']['storage']) || $data['large']['storage'] == IMAGE_STORAGE_DEFAULT) {
    $data['large']['effects'][0]['data']['width'] = 697;
  }

  if (!isset($data['large']['storage']) || $data['large']['storage'] == IMAGE_STORAGE_DEFAULT) {
    $data['large']['effects'][0]['name'] = 'manualcrop_crop_and_scale';
  }

  if (!isset($data['large']['storage']) || $data['large']['storage'] == IMAGE_STORAGE_DEFAULT) {
    $data['large']['label'] = 'large';
  }
  }
  if (isset($data['medium'])) {

  if (!isset($data['medium']['storage']) || $data['medium']['storage'] == IMAGE_STORAGE_DEFAULT) {
    $data['medium']['effects'][0]['data']['height'] = 231;
  }

  if (!isset($data['medium']['storage']) || $data['medium']['storage'] == IMAGE_STORAGE_DEFAULT) {
    $data['medium']['effects'][0]['data']['onlyscaleifcrop'] = 0;
  }

  if (!isset($data['medium']['storage']) || $data['medium']['storage'] == IMAGE_STORAGE_DEFAULT) {
    $data['medium']['effects'][0]['data']['respectminimum'] = 1;
  }

  if (!isset($data['medium']['storage']) || $data['medium']['storage'] == IMAGE_STORAGE_DEFAULT) {
    $data['medium']['effects'][0]['data']['style_name'] = 'medium';
  }

  if (!isset($data['medium']['storage']) || $data['medium']['storage'] == IMAGE_STORAGE_DEFAULT) {
    $data['medium']['effects'][0]['data']['width'] = 370;
  }

  if (!isset($data['medium']['storage']) || $data['medium']['storage'] == IMAGE_STORAGE_DEFAULT) {
    $data['medium']['effects'][0]['name'] = 'manualcrop_crop_and_scale';
  }

  if (!isset($data['medium']['storage']) || $data['medium']['storage'] == IMAGE_STORAGE_DEFAULT) {
    $data['medium']['label'] = 'medium';
  }
  }
  if (isset($data['thumbnail'])) {

  if (!isset($data['thumbnail']['storage']) || $data['thumbnail']['storage'] == IMAGE_STORAGE_DEFAULT) {
    $data['thumbnail']['effects'][0]['data']['height'] = 150;
  }

  if (!isset($data['thumbnail']['storage']) || $data['thumbnail']['storage'] == IMAGE_STORAGE_DEFAULT) {
    $data['thumbnail']['effects'][0]['data']['onlyscaleifcrop'] = 0;
  }

  if (!isset($data['thumbnail']['storage']) || $data['thumbnail']['storage'] == IMAGE_STORAGE_DEFAULT) {
    $data['thumbnail']['effects'][0]['data']['respectminimum'] = 1;
  }

  if (!isset($data['thumbnail']['storage']) || $data['thumbnail']['storage'] == IMAGE_STORAGE_DEFAULT) {
    $data['thumbnail']['effects'][0]['data']['style_name'] = 'thumbnail';
  }

  if (!isset($data['thumbnail']['storage']) || $data['thumbnail']['storage'] == IMAGE_STORAGE_DEFAULT) {
    $data['thumbnail']['effects'][0]['data']['width'] = 240;
  }

  if (!isset($data['thumbnail']['storage']) || $data['thumbnail']['storage'] == IMAGE_STORAGE_DEFAULT) {
    $data['thumbnail']['effects'][0]['name'] = 'manualcrop_crop_and_scale';
  }

  if (!isset($data['thumbnail']['storage']) || $data['thumbnail']['storage'] == IMAGE_STORAGE_DEFAULT) {
    $data['thumbnail']['label'] = 'thumbnail';
  }
  }
}

/**
 * Implements hook_image_default_styles().
 */
function ethical_images_image_default_styles() {
  $styles = array();

  // Exported image style: full.
  $styles['full'] = array(
    'label' => 'full',
    'effects' => array(
      9 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 1170,
          'height' => 658,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'full',
        ),
        'weight' => 0,
      ),
    ),
  );

  return $styles;
}
