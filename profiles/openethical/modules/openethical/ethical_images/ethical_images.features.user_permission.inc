<?php
/**
 * @file
 * ethical_images.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function ethical_images_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer manualcrop settings'.
  $permissions['administer manualcrop settings'] = array(
    'name' => 'administer manualcrop settings',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'manualcrop',
  );

  // Exported permission: 'use manualcrop'.
  $permissions['use manualcrop'] = array(
    'name' => 'use manualcrop',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'manualcrop',
  );

  return $permissions;
}
