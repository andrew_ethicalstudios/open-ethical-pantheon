<?php
/**
 * @file
 * ethical_images.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function ethical_images_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: file_display
  $overrides["file_display.image__default__file_field_file_default.status"] = FALSE;
  $overrides["file_display.image__preview__file_field_file_default.status"] = FALSE;
  $overrides["file_display.image__teaser__file_field_file_default.status"] = FALSE;

  // Exported overrides for: image
  $overrides["image.large.effects|0|data|height"] = 420;
  $overrides["image.large.effects|0|data|onlyscaleifcrop"] = 0;
  $overrides["image.large.effects|0|data|respectminimum"] = 1;
  $overrides["image.large.effects|0|data|style_name"] = 'large';
  $overrides["image.large.effects|0|data|upscale"] = 1;
  $overrides["image.large.effects|0|data|width"] = 697;
  $overrides["image.large.effects|0|name"] = 'manualcrop_crop_and_scale';
  $overrides["image.large.label"] = 'large';
  $overrides["image.medium.effects|0|data|height"] = 231;
  $overrides["image.medium.effects|0|data|onlyscaleifcrop"] = 0;
  $overrides["image.medium.effects|0|data|respectminimum"] = 1;
  $overrides["image.medium.effects|0|data|style_name"] = 'medium';
  $overrides["image.medium.effects|0|data|width"] = 370;
  $overrides["image.medium.effects|0|name"] = 'manualcrop_crop_and_scale';
  $overrides["image.medium.label"] = 'medium';
  $overrides["image.thumbnail.effects|0|data|height"] = 150;
  $overrides["image.thumbnail.effects|0|data|onlyscaleifcrop"] = 0;
  $overrides["image.thumbnail.effects|0|data|respectminimum"] = 1;
  $overrides["image.thumbnail.effects|0|data|style_name"] = 'thumbnail';
  $overrides["image.thumbnail.effects|0|data|width"] = 240;
  $overrides["image.thumbnail.effects|0|name"] = 'manualcrop_crop_and_scale';
  $overrides["image.thumbnail.label"] = 'thumbnail';

 return $overrides;
}
