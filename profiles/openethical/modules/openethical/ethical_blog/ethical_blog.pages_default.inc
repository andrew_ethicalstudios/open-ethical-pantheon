<?php
/**
 * @file
 * ethical_blog.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function ethical_blog_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'openethical_blog';
  $page->task = 'page';
  $page->admin_title = 'Blog';
  $page->admin_description = '';
  $page->path = 'blog';
  $page->access = array(
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array(
    'type' => 'normal',
    'title' => 'Blog',
    'name' => 'menu-secondary-menu',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_openethical_blog_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'openethical_blog';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'radix_burr_flipped';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'sidebar' => NULL,
      'contentmain' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '8325e264-6223-4b94-bc50-d162485126f5';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-66c30398-a456-4910-bb2b-9164bccbcd87';
  $pane->panel = 'contentmain';
  $pane->type = 'views_panes';
  $pane->subtype = 'openethical_blog-panel_pane_1';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'view_mode' => 'featured',
    'widget_title' => NULL,
    'items_per_page' => NULL,
    'exposed' => array(
      'sort_by' => NULL,
      'sort_order' => NULL,
    ),
    'use_pager' => NULL,
    'pager_id' => NULL,
    'offset' => NULL,
    'link_to_view' => NULL,
    'more_link' => NULL,
    'path' => NULL,
    'view_settings' => 'fields',
    'header_type' => 'none',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '66c30398-a456-4910-bb2b-9164bccbcd87';
  $display->content['new-66c30398-a456-4910-bb2b-9164bccbcd87'] = $pane;
  $display->panels['contentmain'][0] = 'new-66c30398-a456-4910-bb2b-9164bccbcd87';
  $pane = new stdClass();
  $pane->pid = 'new-ffc325bd-baee-4f5e-b9be-6a526e6e87e1';
  $pane->panel = 'sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-jr7Y4rdIHBzlnL1Z1WOzaJgnbU2hfnA8';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Project',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'ffc325bd-baee-4f5e-b9be-6a526e6e87e1';
  $display->content['new-ffc325bd-baee-4f5e-b9be-6a526e6e87e1'] = $pane;
  $display->panels['sidebar'][0] = 'new-ffc325bd-baee-4f5e-b9be-6a526e6e87e1';
  $pane = new stdClass();
  $pane->pid = 'new-53c4bdff-1da4-40b8-8bb3-25308d1f787c';
  $pane->panel = 'sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-qfyAI9SB1QwdWFTxpebpEvpV4UjYcKhb';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Category',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '53c4bdff-1da4-40b8-8bb3-25308d1f787c';
  $display->content['new-53c4bdff-1da4-40b8-8bb3-25308d1f787c'] = $pane;
  $display->panels['sidebar'][1] = 'new-53c4bdff-1da4-40b8-8bb3-25308d1f787c';
  $pane = new stdClass();
  $pane->pid = 'new-d49c5f38-817e-4373-b62b-526fd740771d';
  $pane->panel = 'sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-iXnkhNDvZ17T1hi4z8v1gi9Bz15U9HYy';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Country',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = 'd49c5f38-817e-4373-b62b-526fd740771d';
  $display->content['new-d49c5f38-817e-4373-b62b-526fd740771d'] = $pane;
  $display->panels['sidebar'][2] = 'new-d49c5f38-817e-4373-b62b-526fd740771d';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['openethical_blog'] = $page;

  return $pages;

}
