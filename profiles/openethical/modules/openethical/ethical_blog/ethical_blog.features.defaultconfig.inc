<?php
/**
 * @file
 * ethical_blog.features.defaultconfig.inc
 */

/**
 * Implements hook_defaultconfig_features().
 */
function ethical_blog_defaultconfig_features() {
  return array(
    'ethical_blog' => array(
      'user_default_permissions' => 'user_default_permissions',
    ),
  );
}

/**
 * Implements hook_defaultconfig_user_default_permissions().
 */
function ethical_blog_defaultconfig_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create oe_blog_post content'.
  $permissions['create oe_blog_post content'] = array(
    'name' => 'create oe_blog_post content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any oe_blog_post content'.
  $permissions['delete any oe_blog_post content'] = array(
    'name' => 'delete any oe_blog_post content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own oe_blog_post content'.
  $permissions['delete own oe_blog_post content'] = array(
    'name' => 'delete own oe_blog_post content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any oe_blog_post content'.
  $permissions['edit any oe_blog_post content'] = array(
    'name' => 'edit any oe_blog_post content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own oe_blog_post content'.
  $permissions['edit own oe_blog_post content'] = array(
    'name' => 'edit own oe_blog_post content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  return $permissions;
}
