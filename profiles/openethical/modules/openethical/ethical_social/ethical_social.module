<?php
/**
 * @file
 * Drupal needs this blank file.
 */

/**
 * Implementation of hook_menu().
 * A page for adding social info
 */
function ethical_social_menu() {
  $items['admin/config/openethical/social'] = array(
    'title' => 'Social settings',
    'description' => 'Configure Open Ethical Social settings.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ethical_social_settings_form'),
    'access arguments' => array('oe_administer_ethical_social'),
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

function ethical_social_permission() {
  return array(
    'oe_administer_ethical_social' => array(
      'title' => t('Administer Open Ethical Social'),
    ),
  );
}

/**
 * Settings form.
 * Form for adding social media info
 */
function ethical_social_settings_form($form, &$form_state) {
  $form = array();

  $form['ethical_social_url_twitter'] = array(
    '#type' => 'textfield',
    '#title' => t('Twitter'),
    '#default_value' => variable_get('ethical_social_url_twitter', ''),
  );

  $form['ethical_social_url_google'] = array(
    '#type' => 'textfield',
    '#title' => t('Google+'),
    '#default_value' => variable_get('ethical_social_url_google', ''),
  );

  $form['ethical_social_url_facebook'] = array(
    '#type' => 'textfield',
    '#title' => t('Facebook'),
    '#default_value' => variable_get('ethical_social_url_facebook', ''),
  );

  $form['ethical_social_url_linkedin'] = array(
    '#type' => 'textfield',
    '#title' => t('LinkedIn'),
    '#default_value' => variable_get('ethical_social_url_linkedin', ''),
  );

  $form['ethical_social_url_rss'] = array(
    '#type' => 'textfield',
    '#title' => t('RSS Feed'),
    '#default_value' => variable_get('ethical_social_url_rss', ''),
  );

  $form['ethical_social_url_youtube'] = array(
    '#type' => 'textfield',
    '#title' => t('YouTube'),
    '#default_value' => variable_get('ethical_social_url_youtube', ''),
  );

  $form['ethical_social_url_flickr'] = array(
    '#type' => 'textfield',
    '#title' => t('Flickr'),
    '#default_value' => variable_get('ethical_social_url_flickr', ''),
  );

  $form['ethical_social_url_pinterest'] = array(
    '#type' => 'textfield',
    '#title' => t('Pinterest'),
    '#default_value' => variable_get('ethical_social_url_pinterest', ''),
  );

  return system_settings_form($form);
}

/**
 * Implementation of hook_form_alter().
 */
function ethical_social_form_alter(&$form, $form_state, $form_id) {
  //set disqus to be disabled on nodes by default
  if (!empty($form['#node_edit_form'])) {
    $node = $form['#node'];
    $types = variable_get('disqus_nodetypes', array());
    if (isset($types[$node->type]) && !empty($types[$node->type])) {
      $form['comment_settings']['disqus_status']['#default_value'] = FALSE;
    }
  }
}

function ethical_social_format_social_link($text, $url, $icon){
  return '<li>' . l('<i class="' . $icon . '" aria-hidden="true"></i><span>' . $text . '</span>', $url, array('html' => TRUE, )) . '</li>';
}

// Add some markup ready for rendering footer content grabbed from the ethical studios social admin area defined above
// Note that ethical_theme_preprocess_page() also adds some stuff to the footer
function ethical_social_preprocess_page(&$vars){

  $twitter_url = variable_get('ethical_social_url_twitter');
  $facebook_url = variable_get('ethical_social_url_facebook');
  $google_url = variable_get('ethical_social_url_google');
  $rss_url = variable_get('ethical_social_url_rss');
  $linkedin_url = variable_get('ethical_social_url_linkedin');
  $youtube_url = variable_get('ethical_social_url_youtube');
  $flickr_url = variable_get('ethical_social_url_flickr');
  $pinterest_url = variable_get('ethical_social_url_pinterest');

  $social_links = '';

  if(!empty($google_url)){
    $social_links .= ethical_social_format_social_link('Google+', $google_url, 'fa fa-google-plus');
  }

  if(!empty($linkedin_url)){
    $social_links .= ethical_social_format_social_link('LinkedIn', $linkedin_url, 'fa fa-linkedin');
  }

  if(!empty($facebook_url)){
    $social_links .= ethical_social_format_social_link('Facebook', $facebook_url, 'fa fa-facebook');
  }

  if(!empty($rss_url)){
    $social_links .= ethical_social_format_social_link('RSS Feed', $rss_url, 'fa fa-rss');
  }

  if(!empty($twitter_url)){
    $url_bits = explode("/", $twitter_url);
    $twitter_name = $url_bits[sizeof($url_bits) - 1];
    $vars['page']['footer_thirdcolumn']['twitter_feed'] = array('#markup' => '<h3>' . t('Twitter') . '</h3>' . twitter_pull_render('@' . $twitter_name, 'Open Ethical', 3));

    $social_links .= ethical_social_format_social_link('Twitter', $twitter_url, 'fa fa-twitter');
  }

  if(!empty($youtube_url)){
    $social_links .= ethical_social_format_social_link('YouTube', $youtube_url, 'fa fa-youtube');
  }

  if(!empty($flickr_url)){
    $social_links .= ethical_social_format_social_link('Flickr', $flickr_url, 'fa fa-flickr');
  }

  if(!empty($pinterest_url)){
    $social_links .= ethical_social_format_social_link('Pinterest', $pinterest_url, 'fa fa-pinterest');
  }

  if($social_links != ''){
    if(!empty($twitter_url)){
      $vars['page']['footer_secondcolumn']['social_links'] = array('#markup' => '<ul class="social-links">' . $social_links . '</ul>');
    }
    else{
      $vars['page']['footer_thirdcolumn']['social_links'] = array('#markup' => '<h3>' . t('Connect') . '</h3><ul class="social-links">' . $social_links . '</ul>');
    }
  }
}

/**
 * Implements hook_node_view().
 */
function ethical_social_node_view($node, $view_mode) {
  if (isset($node->disqus) && user_access('view disqus comments') && $node->disqus['status'] == 1) {
    switch ($view_mode) {
      case 'teaser':
        // Remove the Disqus link.
        unset($node->content['links']['disqus']);

        break;
      case 'featured':
        // Remove the Disqus link.
        unset($node->content['links']['disqus']);

        break;
    }
  }
}

/**
 * Implementation of hook_ctools_content_subtype_alter()
 */
function ethical_social_ctools_content_subtype_alter(&$subtype, &$plugin) {
  // Modify the Easy Social listing to appear in the generic panelizer IPE content modal sidebar area

  if ($plugin['module'] == 'ctools' && $subtype['title'] == 'Easy Social Block 1') {
    $subtype['category'][0] = 'Custom';
    $subtype['top level'] = TRUE;
    $subtype['title'] = 'Add social links';
    $subtype['icon'] = drupal_get_path('module', 'panopoly_widgets') . '/images/icon_content_list.png';
  }
}
