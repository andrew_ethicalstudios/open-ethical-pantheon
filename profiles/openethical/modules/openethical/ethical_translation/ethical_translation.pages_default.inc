<?php
/**
 * @file
 * ethical_translation.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function ethical_translation_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'oe_blog_ml';
  $page->task = 'page';
  $page->admin_title = 'Blog ML';
  $page->admin_description = '';
  $page->path = 'blog';
  $page->access = array(
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array(
    'type' => 'normal',
    'title' => 'Blog',
    'name' => 'menu-secondary-menu',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_oe_blog_ml_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'oe_blog_ml';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'radix_burr_flipped';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'sidebar' => NULL,
      'contentmain' => NULL,
      'footer' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '8325e264-6223-4b94-bc50-d162485126f5';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-ea72ffb9-95f0-4da9-ad06-10ec84aaec18';
  $pane->panel = 'contentmain';
  $pane->type = 'views_panes';
  $pane->subtype = 'oe_blog_ml-panel_pane_1';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'view_settings' => 'fields',
    'header_type' => 'none',
    'view_mode' => 'teaser',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'ea72ffb9-95f0-4da9-ad06-10ec84aaec18';
  $display->content['new-ea72ffb9-95f0-4da9-ad06-10ec84aaec18'] = $pane;
  $display->panels['contentmain'][0] = 'new-ea72ffb9-95f0-4da9-ad06-10ec84aaec18';
  $pane = new stdClass();
  $pane->pid = 'new-8eaad733-9166-4b87-86c5-cbfecd2816c8';
  $pane->panel = 'sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-yLoOEfqcitV6TMzkyLHYApaYb7cP7zbK';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'site_language',
        'settings' => array(
          'language' => array(
            'en' => 'en',
            'default' => 0,
            'es' => 0,
          ),
        ),
        'not' => FALSE,
      ),
    ),
  );
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Project',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '8eaad733-9166-4b87-86c5-cbfecd2816c8';
  $display->content['new-8eaad733-9166-4b87-86c5-cbfecd2816c8'] = $pane;
  $display->panels['sidebar'][0] = 'new-8eaad733-9166-4b87-86c5-cbfecd2816c8';
  $pane = new stdClass();
  $pane->pid = 'new-8b8bc7a8-d408-455a-bed8-5356107c5be9';
  $pane->panel = 'sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-d0rPjo6fqbtK6V14C1H31LSkvEezYaa3';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'site_language',
        'settings' => array(
          'language' => array(
            'en' => 'en',
            'default' => 0,
            'es' => 0,
          ),
        ),
        'not' => FALSE,
      ),
    ),
  );
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Category',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '8b8bc7a8-d408-455a-bed8-5356107c5be9';
  $display->content['new-8b8bc7a8-d408-455a-bed8-5356107c5be9'] = $pane;
  $display->panels['sidebar'][1] = 'new-8b8bc7a8-d408-455a-bed8-5356107c5be9';
  $pane = new stdClass();
  $pane->pid = 'new-9af6551c-38db-4b49-a146-da05d7d25efa';
  $pane->panel = 'sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-Kvo04VPt9OcOxgYklfACKpQrjHrDfd65';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'site_language',
        'settings' => array(
          'language' => array(
            'es' => 'es',
            'default' => 0,
            'en' => 0,
          ),
        ),
        'not' => FALSE,
      ),
    ),
  );
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'ES Project',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '9af6551c-38db-4b49-a146-da05d7d25efa';
  $display->content['new-9af6551c-38db-4b49-a146-da05d7d25efa'] = $pane;
  $display->panels['sidebar'][2] = 'new-9af6551c-38db-4b49-a146-da05d7d25efa';
  $pane = new stdClass();
  $pane->pid = 'new-480af916-f6a8-4bfe-8af9-ee3ff0be7596';
  $pane->panel = 'sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-h6408hn6bqpi1DSTwn64CIG9pVwMq8aK';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'site_language',
        'settings' => array(
          'language' => array(
            'es' => 'es',
            'default' => 0,
            'en' => 0,
          ),
        ),
        'not' => FALSE,
      ),
    ),
  );
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'ES Category',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = '480af916-f6a8-4bfe-8af9-ee3ff0be7596';
  $display->content['new-480af916-f6a8-4bfe-8af9-ee3ff0be7596'] = $pane;
  $display->panels['sidebar'][3] = 'new-480af916-f6a8-4bfe-8af9-ee3ff0be7596';
  $pane = new stdClass();
  $pane->pid = 'new-a0a26de2-0f5e-48d5-b6f7-b2df540ca338';
  $pane->panel = 'sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-qAAOzwMGDaV90QsM3Z6G4a5JqKF8J8g0';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Country',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 4;
  $pane->locks = array();
  $pane->uuid = 'a0a26de2-0f5e-48d5-b6f7-b2df540ca338';
  $display->content['new-a0a26de2-0f5e-48d5-b6f7-b2df540ca338'] = $pane;
  $display->panels['sidebar'][4] = 'new-a0a26de2-0f5e-48d5-b6f7-b2df540ca338';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['oe_blog_ml'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'oe_document_library_ml';
  $page->task = 'page';
  $page->admin_title = 'Document library ML';
  $page->admin_description = '';
  $page->path = 'documents';
  $page->access = array();
  $page->menu = array(
    'type' => 'normal',
    'title' => 'Library',
    'name' => 'menu-secondary-menu',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_oe_document_library_ml_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'oe_document_library_ml';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'burr_flipped';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'sidebar' => NULL,
      'contentmain' => NULL,
      'footer' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Document library';
  $display->uuid = '8eb16d38-9911-45cb-a019-4294511c9137';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-595ddf95-4f40-4583-87c5-b5672ff4af11';
  $pane->panel = 'contentmain';
  $pane->type = 'views_panes';
  $pane->subtype = 'oe_document_library_ml-panel_pane_1';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'view_settings' => 'fields',
    'header_type' => 'none',
    'view_mode' => 'teaser',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '595ddf95-4f40-4583-87c5-b5672ff4af11';
  $display->content['new-595ddf95-4f40-4583-87c5-b5672ff4af11'] = $pane;
  $display->panels['contentmain'][0] = 'new-595ddf95-4f40-4583-87c5-b5672ff4af11';
  $pane = new stdClass();
  $pane->pid = 'new-a3d2d532-802e-4f76-8adf-e0f91572a939';
  $pane->panel = 'sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-MK6i3WgputSGaZWX20lGB3046quQPAJ1';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Type',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'a3d2d532-802e-4f76-8adf-e0f91572a939';
  $display->content['new-a3d2d532-802e-4f76-8adf-e0f91572a939'] = $pane;
  $display->panels['sidebar'][0] = 'new-a3d2d532-802e-4f76-8adf-e0f91572a939';
  $pane = new stdClass();
  $pane->pid = 'new-fd6f6f64-b320-4850-a2bc-4a99c5968525';
  $pane->panel = 'sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-jT48EsgVBM5blBPB8SPdSaMDqJV8oOk7';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'site_language',
        'settings' => array(
          'language' => array(
            'en' => 'en',
            'default' => 0,
            'es' => 0,
          ),
        ),
        'not' => FALSE,
      ),
    ),
  );
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Project',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'fd6f6f64-b320-4850-a2bc-4a99c5968525';
  $display->content['new-fd6f6f64-b320-4850-a2bc-4a99c5968525'] = $pane;
  $display->panels['sidebar'][1] = 'new-fd6f6f64-b320-4850-a2bc-4a99c5968525';
  $pane = new stdClass();
  $pane->pid = 'new-6cdbe37b-a94b-4518-90a9-7f85c911f2db';
  $pane->panel = 'sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-B0iLfogh2L24OIaldIC0Zk4y2DOkws0i';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'site_language',
        'settings' => array(
          'language' => array(
            'en' => 'en',
            'default' => 0,
            'es' => 0,
          ),
        ),
        'not' => FALSE,
      ),
    ),
  );
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Category',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '6cdbe37b-a94b-4518-90a9-7f85c911f2db';
  $display->content['new-6cdbe37b-a94b-4518-90a9-7f85c911f2db'] = $pane;
  $display->panels['sidebar'][2] = 'new-6cdbe37b-a94b-4518-90a9-7f85c911f2db';
  $pane = new stdClass();
  $pane->pid = 'new-bd920ffd-2f29-4664-a083-0738982bee07';
  $pane->panel = 'sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-jT48EsgVBM5blBPB8SPdSaMDqJV8oOk7';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'site_language',
        'settings' => array(
          'language' => array(
            'es' => 'es',
            'default' => 0,
            'en' => 0,
          ),
        ),
        'not' => FALSE,
      ),
    ),
  );
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'ES Project',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = 'bd920ffd-2f29-4664-a083-0738982bee07';
  $display->content['new-bd920ffd-2f29-4664-a083-0738982bee07'] = $pane;
  $display->panels['sidebar'][3] = 'new-bd920ffd-2f29-4664-a083-0738982bee07';
  $pane = new stdClass();
  $pane->pid = 'new-3071ab19-74c8-41fc-93df-e8f68d8db74e';
  $pane->panel = 'sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-ZBLOWfMWsaDq5ZyIWIGp1NI1E6HxjPIA';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'site_language',
        'settings' => array(
          'language' => array(
            'es' => 'es',
            'default' => 0,
            'en' => 0,
          ),
        ),
        'not' => FALSE,
      ),
    ),
  );
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'ES Category',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 4;
  $pane->locks = array();
  $pane->uuid = '3071ab19-74c8-41fc-93df-e8f68d8db74e';
  $display->content['new-3071ab19-74c8-41fc-93df-e8f68d8db74e'] = $pane;
  $display->panels['sidebar'][4] = 'new-3071ab19-74c8-41fc-93df-e8f68d8db74e';
  $pane = new stdClass();
  $pane->pid = 'new-782e1a8d-c9b7-4135-a2c6-3234a3aae182';
  $pane->panel = 'sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-rtduUbyNNbgGOiQXdst1xlQQW5VwZWAF';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Country',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 5;
  $pane->locks = array();
  $pane->uuid = '782e1a8d-c9b7-4135-a2c6-3234a3aae182';
  $display->content['new-782e1a8d-c9b7-4135-a2c6-3234a3aae182'] = $pane;
  $display->panels['sidebar'][5] = 'new-782e1a8d-c9b7-4135-a2c6-3234a3aae182';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-595ddf95-4f40-4583-87c5-b5672ff4af11';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['oe_document_library_ml'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'oe_events_archive_ml';
  $page->task = 'page';
  $page->admin_title = 'Events archive ML';
  $page->admin_description = '';
  $page->path = 'events/archive';
  $page->access = array(
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array(
    'type' => 'none',
    'title' => '',
    'name' => 'menu-secondary-menu',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_oe_events_archive_ml_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'oe_events_archive_ml';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'burr_flipped';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'sidebar' => NULL,
      'contentmain' => NULL,
      'footer' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Events';
  $display->uuid = 'fd54ef44-4b01-4914-bc79-14132b68e4a2';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-342dfbd1-731c-47a5-b15a-6543a0cb098c';
  $pane->panel = 'contentmain';
  $pane->type = 'ethical_events_nav_pane';
  $pane->subtype = 'ethical_events_nav_pane';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
    'style' => 'bare_bones',
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '342dfbd1-731c-47a5-b15a-6543a0cb098c';
  $display->content['new-342dfbd1-731c-47a5-b15a-6543a0cb098c'] = $pane;
  $display->panels['contentmain'][0] = 'new-342dfbd1-731c-47a5-b15a-6543a0cb098c';
  $pane = new stdClass();
  $pane->pid = 'new-f304a6c8-a50b-42de-b695-fc054dd1efc1';
  $pane->panel = 'contentmain';
  $pane->type = 'views_panes';
  $pane->subtype = 'oe_events_ml-panel_pane_2';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'view_settings' => 'fields',
    'header_type' => 'none',
    'view_mode' => 'teaser',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'f304a6c8-a50b-42de-b695-fc054dd1efc1';
  $display->content['new-f304a6c8-a50b-42de-b695-fc054dd1efc1'] = $pane;
  $display->panels['contentmain'][1] = 'new-f304a6c8-a50b-42de-b695-fc054dd1efc1';
  $pane = new stdClass();
  $pane->pid = 'new-83eaca47-7d40-4ba6-b920-86dd6fcd6bc6';
  $pane->panel = 'sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-iWNVRUry3nK5xE3UA0K1q3tyxtZgwu2t';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'site_language',
        'settings' => array(
          'language' => array(
            'en' => 'en',
            'default' => 0,
            'es' => 0,
          ),
        ),
        'not' => FALSE,
      ),
    ),
  );
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Project',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '83eaca47-7d40-4ba6-b920-86dd6fcd6bc6';
  $display->content['new-83eaca47-7d40-4ba6-b920-86dd6fcd6bc6'] = $pane;
  $display->panels['sidebar'][0] = 'new-83eaca47-7d40-4ba6-b920-86dd6fcd6bc6';
  $pane = new stdClass();
  $pane->pid = 'new-610bab5d-5a9c-40f4-a7ec-ad48ad6f2f08';
  $pane->panel = 'sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-WYY2B7kz00iC1a19egdalczzj7Z0NbcT';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'site_language',
        'settings' => array(
          'language' => array(
            'en' => 'en',
            'default' => 0,
            'es' => 0,
          ),
        ),
        'not' => FALSE,
      ),
    ),
  );
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Category',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '610bab5d-5a9c-40f4-a7ec-ad48ad6f2f08';
  $display->content['new-610bab5d-5a9c-40f4-a7ec-ad48ad6f2f08'] = $pane;
  $display->panels['sidebar'][1] = 'new-610bab5d-5a9c-40f4-a7ec-ad48ad6f2f08';
  $pane = new stdClass();
  $pane->pid = 'new-440beb3e-1ec2-4717-a017-8bd97ebeff45';
  $pane->panel = 'sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-XPzF9lTlv7wJu2SShlkJ4ULd7qaQ4ScC';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'site_language',
        'settings' => array(
          'language' => array(
            'es' => 'es',
            'default' => 0,
            'en' => 0,
          ),
        ),
        'not' => FALSE,
      ),
    ),
  );
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'ES Project',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '440beb3e-1ec2-4717-a017-8bd97ebeff45';
  $display->content['new-440beb3e-1ec2-4717-a017-8bd97ebeff45'] = $pane;
  $display->panels['sidebar'][2] = 'new-440beb3e-1ec2-4717-a017-8bd97ebeff45';
  $pane = new stdClass();
  $pane->pid = 'new-6efd19b8-cfae-444b-a36f-dd25e5fe1607';
  $pane->panel = 'sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-Nc9bjUH0A308oY13lwsKsiIR6n9DND0N';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'site_language',
        'settings' => array(
          'language' => array(
            'es' => 'es',
            'default' => 0,
            'en' => 0,
          ),
        ),
        'not' => FALSE,
      ),
    ),
  );
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'ES Category',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = '6efd19b8-cfae-444b-a36f-dd25e5fe1607';
  $display->content['new-6efd19b8-cfae-444b-a36f-dd25e5fe1607'] = $pane;
  $display->panels['sidebar'][3] = 'new-6efd19b8-cfae-444b-a36f-dd25e5fe1607';
  $pane = new stdClass();
  $pane->pid = 'new-e6c21201-fad9-4f1b-8aec-2c0461bfe8ba';
  $pane->panel = 'sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-oTjPn40nlF9kBaXNx03Vko3RDFZzYutq';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Country',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 4;
  $pane->locks = array();
  $pane->uuid = 'e6c21201-fad9-4f1b-8aec-2c0461bfe8ba';
  $display->content['new-e6c21201-fad9-4f1b-8aec-2c0461bfe8ba'] = $pane;
  $display->panels['sidebar'][4] = 'new-e6c21201-fad9-4f1b-8aec-2c0461bfe8ba';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['oe_events_archive_ml'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'oe_events_ml';
  $page->task = 'page';
  $page->admin_title = 'Events ML';
  $page->admin_description = '';
  $page->path = 'events';
  $page->access = array(
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array(
    'type' => 'normal',
    'title' => 'Events',
    'name' => 'menu-secondary-menu',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_oe_events_ml_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'oe_events_ml';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'burr_flipped';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'sidebar' => NULL,
      'contentmain' => NULL,
      'footer' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'f090081a-2790-4c0d-9842-22143d10ef68';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-fcd35dcb-3f14-467c-997a-41b79271c74e';
  $pane->panel = 'contentmain';
  $pane->type = 'ethical_events_nav_pane';
  $pane->subtype = 'ethical_events_nav_pane';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
    'style' => 'bare_bones',
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'fcd35dcb-3f14-467c-997a-41b79271c74e';
  $display->content['new-fcd35dcb-3f14-467c-997a-41b79271c74e'] = $pane;
  $display->panels['contentmain'][0] = 'new-fcd35dcb-3f14-467c-997a-41b79271c74e';
  $pane = new stdClass();
  $pane->pid = 'new-f489de30-65b7-4f9f-b94b-4290bd17d171';
  $pane->panel = 'contentmain';
  $pane->type = 'views_panes';
  $pane->subtype = 'oe_events_ml-panel_pane_1';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'view_settings' => 'fields',
    'header_type' => 'none',
    'view_mode' => 'teaser',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'f489de30-65b7-4f9f-b94b-4290bd17d171';
  $display->content['new-f489de30-65b7-4f9f-b94b-4290bd17d171'] = $pane;
  $display->panels['contentmain'][1] = 'new-f489de30-65b7-4f9f-b94b-4290bd17d171';
  $pane = new stdClass();
  $pane->pid = 'new-a17bd5c0-6bec-42ea-9d02-78be0a0574c6';
  $pane->panel = 'sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-iWNVRUry3nK5xE3UA0K1q3tyxtZgwu2t';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'site_language',
        'settings' => array(
          'language' => array(
            'en' => 'en',
            'default' => 0,
            'es' => 0,
          ),
        ),
        'not' => FALSE,
      ),
    ),
  );
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Project',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'a17bd5c0-6bec-42ea-9d02-78be0a0574c6';
  $display->content['new-a17bd5c0-6bec-42ea-9d02-78be0a0574c6'] = $pane;
  $display->panels['sidebar'][0] = 'new-a17bd5c0-6bec-42ea-9d02-78be0a0574c6';
  $pane = new stdClass();
  $pane->pid = 'new-f98980b0-3675-425e-921a-d98aa3f43554';
  $pane->panel = 'sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-WYY2B7kz00iC1a19egdalczzj7Z0NbcT';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'site_language',
        'settings' => array(
          'language' => array(
            'en' => 'en',
            'default' => 0,
            'es' => 0,
          ),
        ),
        'not' => FALSE,
      ),
    ),
  );
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Category',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'f98980b0-3675-425e-921a-d98aa3f43554';
  $display->content['new-f98980b0-3675-425e-921a-d98aa3f43554'] = $pane;
  $display->panels['sidebar'][1] = 'new-f98980b0-3675-425e-921a-d98aa3f43554';
  $pane = new stdClass();
  $pane->pid = 'new-f3fe635b-75a7-47c9-83c7-2c7b187162f6';
  $pane->panel = 'sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-XPzF9lTlv7wJu2SShlkJ4ULd7qaQ4ScC';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'site_language',
        'settings' => array(
          'language' => array(
            'es' => 'es',
            'default' => 0,
            'en' => 0,
          ),
        ),
        'not' => FALSE,
      ),
    ),
  );
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'ES Project',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = 'f3fe635b-75a7-47c9-83c7-2c7b187162f6';
  $display->content['new-f3fe635b-75a7-47c9-83c7-2c7b187162f6'] = $pane;
  $display->panels['sidebar'][2] = 'new-f3fe635b-75a7-47c9-83c7-2c7b187162f6';
  $pane = new stdClass();
  $pane->pid = 'new-490ab70e-f4e7-4c63-96d7-e8496aa5f3cb';
  $pane->panel = 'sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-Nc9bjUH0A308oY13lwsKsiIR6n9DND0N';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'site_language',
        'settings' => array(
          'language' => array(
            'es' => 'es',
            'default' => 0,
            'en' => 0,
          ),
        ),
        'not' => FALSE,
      ),
    ),
  );
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'ES Category',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = '490ab70e-f4e7-4c63-96d7-e8496aa5f3cb';
  $display->content['new-490ab70e-f4e7-4c63-96d7-e8496aa5f3cb'] = $pane;
  $display->panels['sidebar'][3] = 'new-490ab70e-f4e7-4c63-96d7-e8496aa5f3cb';
  $pane = new stdClass();
  $pane->pid = 'new-c520540d-59df-4a90-a3e9-c2ae5e1aea58';
  $pane->panel = 'sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-oTjPn40nlF9kBaXNx03Vko3RDFZzYutq';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Country',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 4;
  $pane->locks = array();
  $pane->uuid = 'c520540d-59df-4a90-a3e9-c2ae5e1aea58';
  $display->content['new-c520540d-59df-4a90-a3e9-c2ae5e1aea58'] = $pane;
  $display->panels['sidebar'][4] = 'new-c520540d-59df-4a90-a3e9-c2ae5e1aea58';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['oe_events_ml'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'oe_projects_ml';
  $page->task = 'page';
  $page->admin_title = 'Projects ML';
  $page->admin_description = '';
  $page->path = 'projects';
  $page->access = array();
  $page->menu = array(
    'type' => 'normal',
    'title' => 'Projects',
    'name' => 'menu-secondary-menu',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_oe_projects_ml_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'oe_projects_ml';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => 'projects-page',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'burr_flipped';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'sidebar' => NULL,
      'contentmain' => NULL,
      'footer' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Projects';
  $display->uuid = '982eae68-a5ae-4408-a700-114fd582a3bb';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-778c40d6-71e7-49a4-8fc1-cf3490085b15';
  $pane->panel = 'contentmain';
  $pane->type = 'views_panes';
  $pane->subtype = 'oe_projects_ml-panel_pane_1';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'view_settings' => 'fields',
    'header_type' => 'none',
    'view_mode' => 'teaser',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '778c40d6-71e7-49a4-8fc1-cf3490085b15';
  $display->content['new-778c40d6-71e7-49a4-8fc1-cf3490085b15'] = $pane;
  $display->panels['contentmain'][0] = 'new-778c40d6-71e7-49a4-8fc1-cf3490085b15';
  $pane = new stdClass();
  $pane->pid = 'new-a2e1b3ac-e064-4ce3-a74a-fb07eee603a9';
  $pane->panel = 'sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-sNZt07PSV7OGUB7nFSA0mysDaIdW2GvJ';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Status',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'a2e1b3ac-e064-4ce3-a74a-fb07eee603a9';
  $display->content['new-a2e1b3ac-e064-4ce3-a74a-fb07eee603a9'] = $pane;
  $display->panels['sidebar'][0] = 'new-a2e1b3ac-e064-4ce3-a74a-fb07eee603a9';
  $pane = new stdClass();
  $pane->pid = 'new-c15b7739-09e3-441c-ba62-10ac75fc729b';
  $pane->panel = 'sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-rNmfK41O0CwNUmyKDZgmfUKpuUascGpB';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'site_language',
        'settings' => array(
          'language' => array(
            'en' => 'en',
            'default' => 0,
            'es' => 0,
          ),
        ),
        'not' => FALSE,
      ),
    ),
  );
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Category',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'c15b7739-09e3-441c-ba62-10ac75fc729b';
  $display->content['new-c15b7739-09e3-441c-ba62-10ac75fc729b'] = $pane;
  $display->panels['sidebar'][1] = 'new-c15b7739-09e3-441c-ba62-10ac75fc729b';
  $pane = new stdClass();
  $pane->pid = 'new-537bce40-a19c-44b4-ae78-ccd939b075c4';
  $pane->panel = 'sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-z9qWOW46PE2ZJ9ftBTCOFlqCtF8SCmPx';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'site_language',
        'settings' => array(
          'language' => array(
            'es' => 'es',
            'default' => 0,
            'en' => 0,
          ),
        ),
        'not' => FALSE,
      ),
    ),
  );
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'ES Category',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '537bce40-a19c-44b4-ae78-ccd939b075c4';
  $display->content['new-537bce40-a19c-44b4-ae78-ccd939b075c4'] = $pane;
  $display->panels['sidebar'][2] = 'new-537bce40-a19c-44b4-ae78-ccd939b075c4';
  $pane = new stdClass();
  $pane->pid = 'new-09aa46c5-e01d-43d0-a423-3ff70ae62cc8';
  $pane->panel = 'sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-mUcBjocZcowhg9lNVEdW91JgKpz6JuHk';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Country',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = '09aa46c5-e01d-43d0-a423-3ff70ae62cc8';
  $display->content['new-09aa46c5-e01d-43d0-a423-3ff70ae62cc8'] = $pane;
  $display->panels['sidebar'][3] = 'new-09aa46c5-e01d-43d0-a423-3ff70ae62cc8';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['oe_projects_ml'] = $page;

  return $pages;

}
