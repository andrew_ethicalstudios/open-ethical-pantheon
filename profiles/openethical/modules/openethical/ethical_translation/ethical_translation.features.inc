<?php
/**
 * @file
 * ethical_translation.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ethical_translation_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "facetapi" && $api == "facetapi_defaults") {
    return array("version" => "1");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function ethical_translation_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_default_search_api_index().
 */
function ethical_translation_default_search_api_index() {
  $items = array();
  $items['oe_blog_ml'] = entity_import('search_api_index', '{
    "name" : "Blog ML",
    "machine_name" : "oe_blog_ml",
    "description" : null,
    "server" : "database_server",
    "item_type" : "search_api_et_node",
    "options" : {
      "datasource" : { "bundles" : [] },
      "index_directly" : 1,
      "cron_limit" : "50",
      "search_api_et" : {
        "include" : "incomplete",
        "restrict undefined" : 0,
        "fallback language" : ""
      },
      "fields" : {
        "created" : { "type" : "date" },
        "field_country:name" : { "type" : "list\\u003Cstring\\u003E" },
        "field_featured_categories:name" : { "type" : "list\\u003Cstring\\u003E" },
        "field_project:title" : { "type" : "list\\u003Cstring\\u003E" },
        "nid" : { "type" : "integer" },
        "search_api_language" : { "type" : "string" },
        "search_api_oe_category_name_es" : { "type" : "list\\u003Cstring\\u003E" },
        "search_api_oe_project_title_es" : { "type" : "list\\u003Cstring\\u003E" },
        "title_field" : { "type" : "text" }
      },
      "data_alter_callbacks" : {
        "ethical_media_alter_no_terms" : { "status" : 0, "weight" : "-10", "settings" : [] },
        "search_api_alter_bundle_filter" : {
          "status" : 1,
          "weight" : "-10",
          "settings" : { "default" : "0", "bundles" : { "oe_blog_post" : "oe_blog_post" } }
        },
        "search_api_alter_oe_add_category_name_es" : { "status" : 1, "weight" : "0", "settings" : [] },
        "search_api_alter_oe_add_project_title_es" : { "status" : 1, "weight" : "0", "settings" : [] },
        "search_api_alter_oe_add_project_title_fr" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_oe_add_country_fr" : { "status" : 0, "weight" : "0", "settings" : [] },
        "panelizer" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_oe_add_category_name_fr" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_node_access" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_aggregation" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_url" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_viewed_entity" : { "status" : 0, "weight" : "0", "settings" : { "mode" : "full" } },
        "search_api_alter_add_hierarchy" : { "status" : 0, "weight" : "0", "settings" : { "fields" : [] } },
        "search_api_alter_language_control" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "lang_field" : "", "languages" : [] }
        },
        "search_api_alter_node_status" : { "status" : 1, "weight" : "0", "settings" : [] }
      },
      "processors" : {
        "search_api_case_ignore" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "fields" : { "title_field" : true } }
        },
        "search_api_html_filter" : {
          "status" : 0,
          "weight" : "10",
          "settings" : {
            "fields" : { "title_field" : true },
            "title" : 0,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_transliteration" : {
          "status" : 1,
          "weight" : "15",
          "settings" : { "fields" : { "title_field" : true } }
        },
        "search_api_tokenizer" : {
          "status" : 0,
          "weight" : "20",
          "settings" : {
            "fields" : { "title_field" : true },
            "spaces" : "[^[:alnum:]]",
            "ignorable" : "[\\u0027]"
          }
        },
        "search_api_stopwords" : {
          "status" : 0,
          "weight" : "30",
          "settings" : {
            "fields" : { "title_field" : true },
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        },
        "search_api_highlighting" : {
          "status" : 0,
          "weight" : "35",
          "settings" : {
            "prefix" : "\\u003Cstrong\\u003E",
            "suffix" : "\\u003C\\/strong\\u003E",
            "excerpt" : 1,
            "excerpt_length" : "256",
            "exclude_fields" : [],
            "highlight" : "always"
          }
        }
      }
    },
    "enabled" : "1",
    "read_only" : "0"
  }');
  $items['oe_document_library_ml'] = entity_import('search_api_index', '{
    "name" : "Document library ML",
    "machine_name" : "oe_document_library_ml",
    "description" : null,
    "server" : "database_server",
    "item_type" : "search_api_et_file",
    "options" : {
      "datasource" : { "bundles" : [] },
      "index_directly" : 1,
      "cron_limit" : "50",
      "search_api_et" : {
        "include" : "incomplete",
        "restrict undefined" : 0,
        "fallback language" : ""
      },
      "fields" : {
        "field_country:name" : { "type" : "list\\u003Cstring\\u003E" },
        "field_featured_categories:name" : { "type" : "list\\u003Cstring\\u003E" },
        "field_file_author" : { "type" : "text" },
        "field_file_published_date" : { "type" : "date" },
        "field_oe_document_type:name" : { "type" : "string" },
        "field_oe_file_language" : { "type" : "string" },
        "field_project:title" : { "type" : "list\\u003Cstring\\u003E" },
        "filename_field" : { "type" : "text" },
        "language" : { "type" : "string" },
        "search_api_et_id" : { "type" : "string" },
        "search_api_language" : { "type" : "string" },
        "search_api_oe_category_name_es" : { "type" : "list\\u003Ctext\\u003E" },
        "search_api_oe_project_title_es" : { "type" : "list\\u003Ctext\\u003E" },
        "timestamp" : { "type" : "date" }
      },
      "data_alter_callbacks" : {
        "ethical_media_alter_no_terms" : { "status" : 0, "weight" : "-10", "settings" : [] },
        "search_api_alter_bundle_filter" : {
          "status" : 1,
          "weight" : "-10",
          "settings" : { "default" : "0", "bundles" : { "document" : "document" } }
        },
        "search_api_alter_oe_add_category_name_fr" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_oe_add_project_title_es" : { "status" : 1, "weight" : "0", "settings" : [] },
        "search_api_alter_oe_add_project_title_fr" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_oe_add_country_fr" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_oe_add_category_name_es" : { "status" : 1, "weight" : "0", "settings" : [] },
        "search_api_alter_add_hierarchy" : { "status" : 0, "weight" : "0", "settings" : { "fields" : [] } },
        "search_api_alter_add_url" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_aggregation" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_viewed_entity" : { "status" : 0, "weight" : "0", "settings" : { "mode" : "link" } },
        "search_api_alter_language_control" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "lang_field" : "", "languages" : [] }
        }
      },
      "processors" : {
        "search_api_case_ignore" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "fields" : { "filename_field" : true } }
        },
        "search_api_html_filter" : {
          "status" : 0,
          "weight" : "10",
          "settings" : {
            "fields" : { "field_file_author" : true },
            "title" : 0,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_transliteration" : {
          "status" : 1,
          "weight" : "15",
          "settings" : { "fields" : { "filename_field" : true } }
        },
        "search_api_tokenizer" : {
          "status" : 0,
          "weight" : "20",
          "settings" : {
            "fields" : { "field_file_author" : true },
            "spaces" : "[^[:alnum:]]",
            "ignorable" : "[\\u0027]"
          }
        },
        "search_api_stopwords" : {
          "status" : 0,
          "weight" : "30",
          "settings" : {
            "fields" : { "field_file_author" : true },
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        },
        "search_api_highlighting" : {
          "status" : 0,
          "weight" : "35",
          "settings" : {
            "prefix" : "\\u003Cstrong\\u003E",
            "suffix" : "\\u003C\\/strong\\u003E",
            "excerpt" : 1,
            "excerpt_length" : "256",
            "exclude_fields" : [],
            "highlight" : "always"
          }
        }
      }
    },
    "enabled" : "1",
    "read_only" : "0"
  }');
  $items['oe_events_ml'] = entity_import('search_api_index', '{
    "name" : "Events ML",
    "machine_name" : "oe_events_ml",
    "description" : null,
    "server" : "database_server",
    "item_type" : "search_api_et_node",
    "options" : {
      "datasource" : { "bundles" : [] },
      "index_directly" : 1,
      "cron_limit" : "50",
      "search_api_et" : {
        "include" : "incomplete",
        "restrict undefined" : 0,
        "fallback language" : ""
      },
      "fields" : {
        "field_country:name" : { "type" : "list\\u003Cstring\\u003E" },
        "field_featured_categories:name" : { "type" : "list\\u003Cstring\\u003E" },
        "field_field_start_end_date_tz:value" : { "type" : "date" },
        "field_field_start_end_date_tz:value2" : { "type" : "date" },
        "field_project:title" : { "type" : "list\\u003Cstring\\u003E" },
        "nid" : { "type" : "integer" },
        "search_api_language" : { "type" : "string" },
        "search_api_oe_category_name_es" : { "type" : "list\\u003Ctext\\u003E" },
        "search_api_oe_project_title_es" : { "type" : "list\\u003Ctext\\u003E" },
        "title_field" : { "type" : "text" }
      },
      "data_alter_callbacks" : {
        "ethical_media_alter_no_terms" : { "status" : 0, "weight" : "-10", "settings" : [] },
        "search_api_alter_bundle_filter" : {
          "status" : 1,
          "weight" : "-10",
          "settings" : { "default" : "0", "bundles" : { "oe_event" : "oe_event" } }
        },
        "search_api_alter_oe_add_category_name_es" : { "status" : 1, "weight" : "0", "settings" : [] },
        "search_api_alter_oe_add_project_title_es" : { "status" : 1, "weight" : "0", "settings" : [] },
        "search_api_alter_oe_add_project_title_fr" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_oe_add_country_fr" : { "status" : 0, "weight" : "0", "settings" : [] },
        "panelizer" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_oe_add_category_name_fr" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_node_access" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_aggregation" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_url" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_viewed_entity" : { "status" : 0, "weight" : "0", "settings" : { "mode" : "full" } },
        "search_api_alter_add_hierarchy" : { "status" : 0, "weight" : "0", "settings" : { "fields" : [] } },
        "search_api_alter_language_control" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "lang_field" : "", "languages" : [] }
        },
        "search_api_alter_node_status" : { "status" : 1, "weight" : "0", "settings" : [] }
      },
      "processors" : {
        "search_api_case_ignore" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "fields" : { "title_field" : true } }
        },
        "search_api_html_filter" : {
          "status" : 0,
          "weight" : "10",
          "settings" : {
            "fields" : { "title_field" : true },
            "title" : 0,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_transliteration" : {
          "status" : 1,
          "weight" : "15",
          "settings" : { "fields" : { "title_field" : true } }
        },
        "search_api_tokenizer" : {
          "status" : 0,
          "weight" : "20",
          "settings" : {
            "fields" : { "title_field" : true },
            "spaces" : "[^[:alnum:]]",
            "ignorable" : "[\\u0027]"
          }
        },
        "search_api_stopwords" : {
          "status" : 0,
          "weight" : "30",
          "settings" : {
            "fields" : { "title_field" : true },
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        },
        "search_api_highlighting" : {
          "status" : 0,
          "weight" : "35",
          "settings" : {
            "prefix" : "\\u003Cstrong\\u003E",
            "suffix" : "\\u003C\\/strong\\u003E",
            "excerpt" : 1,
            "excerpt_length" : "256",
            "exclude_fields" : [],
            "highlight" : "always"
          }
        }
      }
    },
    "enabled" : "1",
    "read_only" : "0"
  }');
  $items['oe_projects_ml'] = entity_import('search_api_index', '{
    "name" : "Projects ML",
    "machine_name" : "oe_projects_ml",
    "description" : null,
    "server" : "database_server",
    "item_type" : "search_api_et_node",
    "options" : {
      "datasource" : { "bundles" : [] },
      "index_directly" : 1,
      "cron_limit" : "50",
      "search_api_et" : {
        "include" : "incomplete",
        "restrict undefined" : 0,
        "fallback language" : ""
      },
      "fields" : {
        "field_country:name" : { "type" : "list\\u003Cstring\\u003E" },
        "field_featured_categories:name" : { "type" : "list\\u003Cstring\\u003E" },
        "field_project_status" : { "type" : "string" },
        "field_start_end_day:value" : { "type" : "date" },
        "field_start_end_day:value2" : { "type" : "date" },
        "nid" : { "type" : "integer" },
        "search_api_language" : { "type" : "string" },
        "search_api_oe_category_name_es" : { "type" : "list\\u003Ctext\\u003E" },
        "title_field" : { "type" : "text" }
      },
      "data_alter_callbacks" : {
        "ethical_media_alter_no_terms" : { "status" : 0, "weight" : "-10", "settings" : [] },
        "search_api_alter_bundle_filter" : {
          "status" : 1,
          "weight" : "-10",
          "settings" : { "default" : "0", "bundles" : { "oe_project" : "oe_project" } }
        },
        "search_api_alter_oe_add_category_name_es" : { "status" : 1, "weight" : "0", "settings" : [] },
        "search_api_alter_oe_add_project_title_es" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_oe_add_project_title_fr" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_oe_add_country_fr" : { "status" : 0, "weight" : "0", "settings" : [] },
        "panelizer" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_oe_add_category_name_fr" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_node_access" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_aggregation" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_url" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_viewed_entity" : { "status" : 0, "weight" : "0", "settings" : { "mode" : "full" } },
        "search_api_alter_add_hierarchy" : { "status" : 0, "weight" : "0", "settings" : { "fields" : [] } },
        "search_api_alter_language_control" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "lang_field" : "", "languages" : [] }
        },
        "search_api_alter_node_status" : { "status" : 1, "weight" : "0", "settings" : [] }
      },
      "processors" : {
        "search_api_case_ignore" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "fields" : { "title_field" : true } }
        },
        "search_api_html_filter" : {
          "status" : 0,
          "weight" : "10",
          "settings" : {
            "fields" : { "title_field" : true },
            "title" : 0,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_transliteration" : {
          "status" : 1,
          "weight" : "15",
          "settings" : { "fields" : { "title_field" : true } }
        },
        "search_api_tokenizer" : {
          "status" : 0,
          "weight" : "20",
          "settings" : {
            "fields" : { "title_field" : true },
            "spaces" : "[^[:alnum:]]",
            "ignorable" : "[\\u0027]"
          }
        },
        "search_api_stopwords" : {
          "status" : 0,
          "weight" : "30",
          "settings" : {
            "fields" : { "title_field" : true },
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        },
        "search_api_highlighting" : {
          "status" : 0,
          "weight" : "35",
          "settings" : {
            "prefix" : "\\u003Cstrong\\u003E",
            "suffix" : "\\u003C\\/strong\\u003E",
            "excerpt" : 1,
            "excerpt_length" : "256",
            "exclude_fields" : [],
            "highlight" : "always"
          }
        }
      }
    },
    "enabled" : "1",
    "read_only" : "0"
  }');
  return $items;
}
