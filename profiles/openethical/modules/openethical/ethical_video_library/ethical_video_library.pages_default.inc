<?php
/**
 * @file
 * ethical_video_library.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function ethical_video_library_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'openethical_video_library';
  $page->task = 'page';
  $page->admin_title = 'Video library';
  $page->admin_description = '';
  $page->path = 'videos';
  $page->access = array(
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array(
    'type' => 'normal',
    'title' => 'Videos',
    'name' => 'menu-secondary-menu',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_openethical_video_library_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'openethical_video_library';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'burr_flipped';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'sidebar' => NULL,
      'contentmain' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'abadf666-969a-4a14-a6ec-f63592cc3a87';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-1c242411-57b4-4ab0-82d5-727bffa2ee2b';
  $pane->panel = 'contentmain';
  $pane->type = 'views_panes';
  $pane->subtype = 'openethical_video_library-panel_pane_1';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'view_mode' => 'teaser',
    'widget_title' => NULL,
    'items_per_page' => NULL,
    'exposed' => array(
      'sort_by' => NULL,
      'sort_order' => NULL,
    ),
    'use_pager' => NULL,
    'pager_id' => NULL,
    'offset' => NULL,
    'link_to_view' => NULL,
    'more_link' => NULL,
    'path' => NULL,
    'view_settings' => 'fields',
    'header_type' => 'none',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '1c242411-57b4-4ab0-82d5-727bffa2ee2b';
  $display->content['new-1c242411-57b4-4ab0-82d5-727bffa2ee2b'] = $pane;
  $display->panels['contentmain'][0] = 'new-1c242411-57b4-4ab0-82d5-727bffa2ee2b';
  $pane = new stdClass();
  $pane->pid = 'new-0fcc482f-bdb3-4d4a-a90b-3fa6b6c6b4a9';
  $pane->panel = 'sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-LMt4JvidlUG0bvEXOZLx1Rtp1AkrI0kW';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Project',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '0fcc482f-bdb3-4d4a-a90b-3fa6b6c6b4a9';
  $display->content['new-0fcc482f-bdb3-4d4a-a90b-3fa6b6c6b4a9'] = $pane;
  $display->panels['sidebar'][0] = 'new-0fcc482f-bdb3-4d4a-a90b-3fa6b6c6b4a9';
  $pane = new stdClass();
  $pane->pid = 'new-a86f8a83-ac4d-4e51-9680-cd72f1ce1261';
  $pane->panel = 'sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-OCBXewRzqyZAzY1RrVmgkz3D1f73pVVg';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Category',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'a86f8a83-ac4d-4e51-9680-cd72f1ce1261';
  $display->content['new-a86f8a83-ac4d-4e51-9680-cd72f1ce1261'] = $pane;
  $display->panels['sidebar'][1] = 'new-a86f8a83-ac4d-4e51-9680-cd72f1ce1261';
  $pane = new stdClass();
  $pane->pid = 'new-88b12312-de67-4ae2-b960-61d9d3c43cee';
  $pane->panel = 'sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-QS8SPORMcvaAU5vAqGGDHCaWtbznc5qS';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Country',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '88b12312-de67-4ae2-b960-61d9d3c43cee';
  $display->content['new-88b12312-de67-4ae2-b960-61d9d3c43cee'] = $pane;
  $display->panels['sidebar'][2] = 'new-88b12312-de67-4ae2-b960-61d9d3c43cee';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['openethical_video_library'] = $page;

  return $pages;

}
