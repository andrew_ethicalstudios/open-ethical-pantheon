(function ($) {
  
  Drupal.behaviors.EthicalDocumentLibraryTruncator = {
    attach: function (context, settings) {
      jQuery().ready(function() {  
        $('.oe-portrait-search-result .oe-basic .description').truncate({
          max_length: 250,
          more: 'more'
        });  
      }); 
    }    
  };
 
})(jQuery);

