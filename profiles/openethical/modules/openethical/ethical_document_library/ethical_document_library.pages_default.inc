<?php
/**
 * @file
 * ethical_document_library.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function ethical_document_library_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'openethical_document_library';
  $page->task = 'page';
  $page->admin_title = 'openethical_document_library';
  $page->admin_description = '';
  $page->path = 'documents';
  $page->access = array(
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array(
    'type' => 'normal',
    'title' => 'Library',
    'name' => 'menu-secondary-menu',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_openethical_document_library_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'openethical_document_library';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'radix_burr_flipped';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'sidebar' => NULL,
      'contentmain' => NULL,
      'footer' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Document library';
  $display->uuid = 'c11d5e05-e8f4-4e4c-be34-7efea3437d97';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-1047d32b-e994-4625-90c0-d56ef3c8a450';
  $pane->panel = 'contentmain';
  $pane->type = 'views_panes';
  $pane->subtype = 'open_ethical_document_library-panel_pane_1';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'view_mode' => 'teaser',
    'widget_title' => NULL,
    'items_per_page' => NULL,
    'exposed' => array(
      'sort_by' => NULL,
      'sort_order' => NULL,
    ),
    'use_pager' => NULL,
    'pager_id' => NULL,
    'offset' => NULL,
    'link_to_view' => NULL,
    'more_link' => NULL,
    'path' => NULL,
    'view_settings' => 'fields',
    'header_type' => 'none',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '1047d32b-e994-4625-90c0-d56ef3c8a450';
  $display->content['new-1047d32b-e994-4625-90c0-d56ef3c8a450'] = $pane;
  $display->panels['contentmain'][0] = 'new-1047d32b-e994-4625-90c0-d56ef3c8a450';
  $pane = new stdClass();
  $pane->pid = 'new-2e907c25-68dc-4c98-a75d-d8532d5ded43';
  $pane->panel = 'sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-yV5l86P6byDww5VQbRJLG1uTRZCq1ifN';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Type',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '2e907c25-68dc-4c98-a75d-d8532d5ded43';
  $display->content['new-2e907c25-68dc-4c98-a75d-d8532d5ded43'] = $pane;
  $display->panels['sidebar'][0] = 'new-2e907c25-68dc-4c98-a75d-d8532d5ded43';
  $pane = new stdClass();
  $pane->pid = 'new-35b22744-5f0e-429a-98c8-22f18a4f337c';
  $pane->panel = 'sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-xXpq1LyhgCIxOlLkQTC4FE8mwcWyOZ9M';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Category',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '35b22744-5f0e-429a-98c8-22f18a4f337c';
  $display->content['new-35b22744-5f0e-429a-98c8-22f18a4f337c'] = $pane;
  $display->panels['sidebar'][1] = 'new-35b22744-5f0e-429a-98c8-22f18a4f337c';
  $pane = new stdClass();
  $pane->pid = 'new-957ca941-4b53-4024-a588-f5c6ede7afcd';
  $pane->panel = 'sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-D9RadUnk1913V2Ed4QTmsAfs4onZ1D2J';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Country',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '957ca941-4b53-4024-a588-f5c6ede7afcd';
  $display->content['new-957ca941-4b53-4024-a588-f5c6ede7afcd'] = $pane;
  $display->panels['sidebar'][2] = 'new-957ca941-4b53-4024-a588-f5c6ede7afcd';
  $pane = new stdClass();
  $pane->pid = 'new-b921c352-e367-48f5-9704-62d4e81337df';
  $pane->panel = 'sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-T60VEwe0NvKglWuTRoi0mWccfVPAYNiE';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Project',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = 'b921c352-e367-48f5-9704-62d4e81337df';
  $display->content['new-b921c352-e367-48f5-9704-62d4e81337df'] = $pane;
  $display->panels['sidebar'][3] = 'new-b921c352-e367-48f5-9704-62d4e81337df';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['openethical_document_library'] = $page;

  return $pages;

}
