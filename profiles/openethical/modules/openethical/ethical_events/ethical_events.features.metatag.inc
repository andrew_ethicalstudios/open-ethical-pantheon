<?php
/**
 * @file
 * ethical_events.features.metatag.inc
 */

/**
 * Implements hook_metatag_export_default().
 */
function ethical_events_metatag_export_default() {
  $config = array();

  // Exported Metatag config instance: node:oe_event.
  $config['node:oe_event'] = array(
    'instance' => 'node:oe_event',
    'config' => array(
      'image_src' => array(
        'value' => '[node:field_featured_image]',
      ),
      'og:image' => array(
        'value' => '[node:field_featured_image]',
      ),
      'og:street-address' => array(
        'value' => '[node:field_map_address]',
      ),
    ),
  );

  return $config;
}
