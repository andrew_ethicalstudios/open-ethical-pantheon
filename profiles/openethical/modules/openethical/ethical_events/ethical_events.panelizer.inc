<?php
/**
 * @file
 * ethical_events.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function ethical_events_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:oe_event:default';
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'oe_event';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = 'event-page';
  $panelizer->css = '';
  $panelizer->pipeline = 'ipe';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array(
    'panels_breadcrumbs_state' => 0,
    'panels_breadcrumbs_titles' => 'Events',
    'panels_breadcrumbs_paths' => 'events',
    'panels_breadcrumbs_home' => 1,
  );
  $display = new panels_display();
  $display->layout = 'radix_burr_flipped';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'sidebar' => NULL,
      'contentmain' => NULL,
      'footer' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = 'ff016652-6c72-4bd7-9a98-0da580077125';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-5a32d08c-71bd-4f55-82a5-9026924117f9';
  $pane->panel = 'contentmain';
  $pane->type = 'panels_mini';
  $pane->subtype = 'oe_event_meta_data';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => array(
      0 => 'panelizer',
    ),
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '5a32d08c-71bd-4f55-82a5-9026924117f9';
  $display->content['new-5a32d08c-71bd-4f55-82a5-9026924117f9'] = $pane;
  $display->panels['contentmain'][0] = 'new-5a32d08c-71bd-4f55-82a5-9026924117f9';
  $pane = new stdClass();
  $pane->pid = 'new-838a17da-b237-4d5c-a667-036652f5aaf3';
  $pane->panel = 'contentmain';
  $pane->type = 'panels_mini';
  $pane->subtype = 'oe_event_main_content';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => array(
      0 => 'panelizer',
    ),
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '838a17da-b237-4d5c-a667-036652f5aaf3';
  $display->content['new-838a17da-b237-4d5c-a667-036652f5aaf3'] = $pane;
  $display->panels['contentmain'][1] = 'new-838a17da-b237-4d5c-a667-036652f5aaf3';
  $pane = new stdClass();
  $pane->pid = 'new-5eff8eb3-d6b5-4a0a-a01c-239e81c8bf2f';
  $pane->panel = 'contentmain';
  $pane->type = 'block';
  $pane->subtype = 'easy_social-easy_social_block_1';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array(
    'type' => 'none',
    'regions' => array(
      'contentmain' => 'contentmain',
      'footer' => 'footer',
    ),
  );
  $pane->uuid = '5eff8eb3-d6b5-4a0a-a01c-239e81c8bf2f';
  $display->content['new-5eff8eb3-d6b5-4a0a-a01c-239e81c8bf2f'] = $pane;
  $display->panels['contentmain'][2] = 'new-5eff8eb3-d6b5-4a0a-a01c-239e81c8bf2f';
  $pane = new stdClass();
  $pane->pid = 'new-53c59e4f-3f5f-42c5-81d4-201d3ba838f7';
  $pane->panel = 'contentmain';
  $pane->type = 'panels_mini';
  $pane->subtype = 'oe_event_tags';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => array(
      0 => 'panelizer',
    ),
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = '53c59e4f-3f5f-42c5-81d4-201d3ba838f7';
  $display->content['new-53c59e4f-3f5f-42c5-81d4-201d3ba838f7'] = $pane;
  $display->panels['contentmain'][3] = 'new-53c59e4f-3f5f-42c5-81d4-201d3ba838f7';
  $pane = new stdClass();
  $pane->pid = 'new-db5fc56a-0dc6-4867-b965-eab12e98e49a';
  $pane->panel = 'footer';
  $pane->type = 'panels_mini';
  $pane->subtype = 'oe_event_footer';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => array(
      0 => 'panelizer',
    ),
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'db5fc56a-0dc6-4867-b965-eab12e98e49a';
  $display->content['new-db5fc56a-0dc6-4867-b965-eab12e98e49a'] = $pane;
  $display->panels['footer'][0] = 'new-db5fc56a-0dc6-4867-b965-eab12e98e49a';
  $pane = new stdClass();
  $pane->pid = 'new-13001038-2388-458b-8937-d47b25f37f51';
  $pane->panel = 'sidebar';
  $pane->type = 'panels_mini';
  $pane->subtype = 'oe_event_side_bar';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => array(
      0 => 'panelizer',
    ),
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '13001038-2388-458b-8937-d47b25f37f51';
  $display->content['new-13001038-2388-458b-8937-d47b25f37f51'] = $pane;
  $display->panels['sidebar'][0] = 'new-13001038-2388-458b-8937-d47b25f37f51';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:oe_event:default'] = $panelizer;

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:oe_event:default:default';
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'oe_event';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'default';
  $panelizer->css_class = 'event-default';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = 'cd7070b0-44f9-498b-9d08-fc7077a70779';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-751a4bfe-a21e-4f88-955e-d40de4170b2b';
  $pane->panel = 'center';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:body';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'text_default',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(),
    'context' => 'panelizer',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '751a4bfe-a21e-4f88-955e-d40de4170b2b';
  $display->content['new-751a4bfe-a21e-4f88-955e-d40de4170b2b'] = $pane;
  $display->panels['center'][0] = 'new-751a4bfe-a21e-4f88-955e-d40de4170b2b';
  $pane = new stdClass();
  $pane->pid = 'new-08c0afcf-e1f0-4b1a-b83f-f1bb1799bb3b';
  $pane->panel = 'center';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_featured_image';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'above',
    'formatter' => 'image',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(
      'image_style' => '',
      'image_link' => '',
    ),
    'context' => 'panelizer',
    'override_title' => 0,
    'override_title_text' => '',
    'view_mode' => NULL,
    'widget_title' => NULL,
    'items_per_page' => NULL,
    'exposed' => array(
      'sort_by' => NULL,
      'sort_order' => NULL,
    ),
    'use_pager' => NULL,
    'pager_id' => NULL,
    'offset' => NULL,
    'link_to_view' => NULL,
    'more_link' => NULL,
    'path' => NULL,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '08c0afcf-e1f0-4b1a-b83f-f1bb1799bb3b';
  $display->content['new-08c0afcf-e1f0-4b1a-b83f-f1bb1799bb3b'] = $pane;
  $display->panels['center'][1] = 'new-08c0afcf-e1f0-4b1a-b83f-f1bb1799bb3b';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:oe_event:default:default'] = $panelizer;

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:oe_event:default:featured';
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'oe_event';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'featured';
  $panelizer->css_class = 'event-featured';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'oe_basic';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'sidebar' => NULL,
      'contentmain' => NULL,
      'header' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '92f85f4b-111c-41c5-9a20-8bc4bd4fd3e1';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-7ccb2775-1ea3-49f8-a0f2-58adcab397b1';
  $pane->panel = 'contentmain';
  $pane->type = 'node_title';
  $pane->subtype = 'node_title';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'link' => 1,
    'markup' => 'h3',
    'id' => '',
    'class' => '',
    'context' => 'panelizer',
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '7ccb2775-1ea3-49f8-a0f2-58adcab397b1';
  $display->content['new-7ccb2775-1ea3-49f8-a0f2-58adcab397b1'] = $pane;
  $display->panels['contentmain'][0] = 'new-7ccb2775-1ea3-49f8-a0f2-58adcab397b1';
  $pane = new stdClass();
  $pane->pid = 'new-71318611-78ba-4fa8-8ea0-094ed25238d1';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_field_start_end_date_tz';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'ethical_event_date',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(
      'format_type' => 'medium',
      'multiple_number' => '',
      'multiple_from' => '',
      'multiple_to' => '',
      'fromto' => 'both',
    ),
    'context' => array(),
    'override_title' => 0,
    'override_title_text' => '',
    'view_mode' => NULL,
    'widget_title' => NULL,
    'items_per_page' => NULL,
    'exposed' => array(
      'sort_by' => NULL,
      'sort_order' => NULL,
    ),
    'use_pager' => NULL,
    'pager_id' => NULL,
    'offset' => NULL,
    'link_to_view' => NULL,
    'more_link' => NULL,
    'path' => NULL,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
    'style' => 'default',
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '71318611-78ba-4fa8-8ea0-094ed25238d1';
  $display->content['new-71318611-78ba-4fa8-8ea0-094ed25238d1'] = $pane;
  $display->panels['contentmain'][1] = 'new-71318611-78ba-4fa8-8ea0-094ed25238d1';
  $pane = new stdClass();
  $pane->pid = 'new-676c9892-9c63-4ec0-a378-c8af21a4dc63';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_map_address';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'text_default',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(),
    'context' => array(),
    'override_title' => 0,
    'override_title_text' => '',
    'view_mode' => NULL,
    'widget_title' => NULL,
    'items_per_page' => NULL,
    'exposed' => array(
      'sort_by' => NULL,
      'sort_order' => NULL,
    ),
    'use_pager' => NULL,
    'pager_id' => NULL,
    'offset' => NULL,
    'link_to_view' => NULL,
    'more_link' => NULL,
    'path' => NULL,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
    'style' => 'default',
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '676c9892-9c63-4ec0-a378-c8af21a4dc63';
  $display->content['new-676c9892-9c63-4ec0-a378-c8af21a4dc63'] = $pane;
  $display->panels['contentmain'][2] = 'new-676c9892-9c63-4ec0-a378-c8af21a4dc63';
  $pane = new stdClass();
  $pane->pid = 'new-b0481845-2609-4042-8e1c-90c8e60bee1a';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:body';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'smart_trim_format',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(
      'trim_length' => '25',
      'trim_type' => 'words',
      'trim_suffix' => '...',
      'more_link' => '0',
      'more_text' => 'Read more',
      'summary_handler' => 'full',
      'trim_options' => array(
        'text' => 0,
      ),
    ),
    'context' => array(),
    'override_title' => 0,
    'override_title_text' => '',
    'view_mode' => NULL,
    'widget_title' => NULL,
    'items_per_page' => NULL,
    'exposed' => array(
      'sort_by' => NULL,
      'sort_order' => NULL,
    ),
    'use_pager' => NULL,
    'pager_id' => NULL,
    'offset' => NULL,
    'link_to_view' => NULL,
    'more_link' => NULL,
    'path' => NULL,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = 'b0481845-2609-4042-8e1c-90c8e60bee1a';
  $display->content['new-b0481845-2609-4042-8e1c-90c8e60bee1a'] = $pane;
  $display->panels['contentmain'][3] = 'new-b0481845-2609-4042-8e1c-90c8e60bee1a';
  $pane = new stdClass();
  $pane->pid = 'new-08c01583-703b-49d7-a772-e67069e81dfe';
  $pane->panel = 'contentmain';
  $pane->type = 'node_links';
  $pane->subtype = 'node_links';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
    'build_mode' => 'teaser',
    'identifier' => '',
    'link' => 1,
    'context' => 'panelizer',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_class' => 'link-wrapper',
  );
  $pane->extras = array();
  $pane->position = 4;
  $pane->locks = array();
  $pane->uuid = '08c01583-703b-49d7-a772-e67069e81dfe';
  $display->content['new-08c01583-703b-49d7-a772-e67069e81dfe'] = $pane;
  $display->panels['contentmain'][4] = 'new-08c01583-703b-49d7-a772-e67069e81dfe';
  $pane = new stdClass();
  $pane->pid = 'new-d4582e76-23c4-4124-92f5-204ce5367bd9';
  $pane->panel = 'sidebar';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_featured_image';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'image',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(
      'image_style' => 'medium',
      'image_link' => 'content',
    ),
    'context' => array(),
    'override_title' => 0,
    'override_title_text' => '',
    'view_mode' => NULL,
    'widget_title' => NULL,
    'items_per_page' => NULL,
    'exposed' => array(
      'sort_by' => NULL,
      'sort_order' => NULL,
    ),
    'use_pager' => NULL,
    'pager_id' => NULL,
    'offset' => NULL,
    'link_to_view' => NULL,
    'more_link' => NULL,
    'path' => NULL,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'd4582e76-23c4-4124-92f5-204ce5367bd9';
  $display->content['new-d4582e76-23c4-4124-92f5-204ce5367bd9'] = $pane;
  $display->panels['sidebar'][0] = 'new-d4582e76-23c4-4124-92f5-204ce5367bd9';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:oe_event:default:featured'] = $panelizer;

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:oe_event:default:teaser';
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'oe_event';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'teaser';
  $panelizer->css_class = 'event-teaser';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'oe_basic';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'sidebar' => NULL,
      'contentmain' => NULL,
      'header' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '853ad248-c2c1-43d4-ae70-f8b908476581';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-4e46873d-d587-4600-a0ff-65b08bb5af71';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_field_start_end_date_tz';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'ethical_event_date',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(
      'format_type' => 'medium',
      'multiple_number' => '',
      'multiple_from' => '',
      'multiple_to' => '',
      'fromto' => 'both',
    ),
    'context' => array(),
    'override_title' => 0,
    'override_title_text' => '',
    'view_mode' => NULL,
    'widget_title' => NULL,
    'items_per_page' => NULL,
    'exposed' => array(
      'sort_by' => NULL,
      'sort_order' => NULL,
    ),
    'use_pager' => NULL,
    'pager_id' => NULL,
    'offset' => NULL,
    'link_to_view' => NULL,
    'more_link' => NULL,
    'path' => NULL,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '4e46873d-d587-4600-a0ff-65b08bb5af71';
  $display->content['new-4e46873d-d587-4600-a0ff-65b08bb5af71'] = $pane;
  $display->panels['contentmain'][0] = 'new-4e46873d-d587-4600-a0ff-65b08bb5af71';
  $pane = new stdClass();
  $pane->pid = 'new-9f9b4abb-1264-438c-b1d6-9f569f428c1e';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:body';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'smart_trim_format',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(
      'trim_length' => '25',
      'trim_type' => 'words',
      'trim_suffix' => '...',
      'more_link' => '0',
      'more_text' => 'Read more',
      'summary_handler' => 'trim',
      'trim_options' => array(
        'text' => 0,
      ),
    ),
    'context' => 'panelizer',
    'override_title' => 0,
    'override_title_text' => '',
    'view_mode' => NULL,
    'widget_title' => NULL,
    'items_per_page' => NULL,
    'exposed' => array(
      'sort_by' => NULL,
      'sort_order' => NULL,
    ),
    'use_pager' => NULL,
    'pager_id' => NULL,
    'offset' => NULL,
    'link_to_view' => NULL,
    'more_link' => NULL,
    'path' => NULL,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '9f9b4abb-1264-438c-b1d6-9f569f428c1e';
  $display->content['new-9f9b4abb-1264-438c-b1d6-9f569f428c1e'] = $pane;
  $display->panels['contentmain'][1] = 'new-9f9b4abb-1264-438c-b1d6-9f569f428c1e';
  $pane = new stdClass();
  $pane->pid = 'new-84565f01-2a58-44c4-a3f1-51ff23a1bbb1';
  $pane->panel = 'contentmain';
  $pane->type = 'node_links';
  $pane->subtype = 'node_links';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => FALSE,
    'override_title_text' => '',
    'build_mode' => 'teaser',
    'identifier' => '',
    'link' => TRUE,
    'context' => 'panelizer',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_class' => 'link-wrapper',
  );
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '84565f01-2a58-44c4-a3f1-51ff23a1bbb1';
  $display->content['new-84565f01-2a58-44c4-a3f1-51ff23a1bbb1'] = $pane;
  $display->panels['contentmain'][2] = 'new-84565f01-2a58-44c4-a3f1-51ff23a1bbb1';
  $pane = new stdClass();
  $pane->pid = 'new-396367ec-6cff-4793-bc1c-32e6e0f92c10';
  $pane->panel = 'header';
  $pane->type = 'node_title';
  $pane->subtype = 'node_title';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'link' => 1,
    'markup' => 'h3',
    'id' => '',
    'class' => '',
    'context' => 'panelizer',
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '396367ec-6cff-4793-bc1c-32e6e0f92c10';
  $display->content['new-396367ec-6cff-4793-bc1c-32e6e0f92c10'] = $pane;
  $display->panels['header'][0] = 'new-396367ec-6cff-4793-bc1c-32e6e0f92c10';
  $pane = new stdClass();
  $pane->pid = 'new-d0d5a4e6-c525-4476-8b7d-d83c6f0d4785';
  $pane->panel = 'sidebar';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_featured_image';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'image',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(
      'image_style' => 'thumbnail',
      'image_link' => 'content',
    ),
    'context' => 'panelizer',
    'override_title' => 0,
    'override_title_text' => '',
    'view_mode' => NULL,
    'widget_title' => NULL,
    'items_per_page' => NULL,
    'exposed' => array(
      'sort_by' => NULL,
      'sort_order' => NULL,
    ),
    'use_pager' => NULL,
    'pager_id' => NULL,
    'offset' => NULL,
    'link_to_view' => NULL,
    'more_link' => NULL,
    'path' => NULL,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'd0d5a4e6-c525-4476-8b7d-d83c6f0d4785';
  $display->content['new-d0d5a4e6-c525-4476-8b7d-d83c6f0d4785'] = $pane;
  $display->panels['sidebar'][0] = 'new-d0d5a4e6-c525-4476-8b7d-d83c6f0d4785';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:oe_event:default:teaser'] = $panelizer;

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:oe_event:default:tiny';
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'oe_event';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'tiny';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'oe_empty';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'contentmain' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '662546a8-bd44-46b8-b449-b13385977924';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-c20b8c4b-a845-4579-9d8d-4bd91bf3a34e';
  $pane->panel = 'contentmain';
  $pane->type = 'node_title';
  $pane->subtype = 'node_title';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'link' => 1,
    'markup' => 'none',
    'id' => '',
    'class' => '',
    'context' => 'panelizer',
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'c20b8c4b-a845-4579-9d8d-4bd91bf3a34e';
  $display->content['new-c20b8c4b-a845-4579-9d8d-4bd91bf3a34e'] = $pane;
  $display->panels['contentmain'][0] = 'new-c20b8c4b-a845-4579-9d8d-4bd91bf3a34e';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:oe_event:default:tiny'] = $panelizer;

  return $export;
}
