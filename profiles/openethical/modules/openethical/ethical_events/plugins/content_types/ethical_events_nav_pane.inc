<?php
/**
* This plugin array is more or less self documenting
*/
$plugin = array(
  // the title in the admin
  'title' => t('Ethical events nav pane'),
  // no one knows if "single" defaults to FALSE...
  'single' => TRUE,
  // oh joy, I get my own section of panel panes
  'category' => array(t('Admin'), -9),
  'render callback' => 'ethical_events_nav_pane_content_type_render'
);


/**
* Run-time rendering of the body of the block (content type)
* See ctools_plugin_examples for more advanced info
*/
function ethical_events_nav_pane_content_type_render($subtype, $conf, $context = NULL) {

  $block = new stdClass();

  if(arg(0) == 'events' && arg(1) == 'archive'){
    $block->content = '<p class="mini-nav"><a href="../events">' . t('Up & coming') . '</a> / <span>' . t('Archive') . '</span></p>';
  }
  elseif(arg(0) == 'events'){
    $block->content = '<p class="mini-nav"><span>' . t('Up & coming') . '</span> / <a href="events/archive">' . t('Archive') . '</a></p>';
  }

  return $block;
}


