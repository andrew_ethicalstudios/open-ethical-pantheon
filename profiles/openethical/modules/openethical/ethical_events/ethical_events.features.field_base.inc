<?php
/**
 * @file
 * ethical_events.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function ethical_events_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_event_display_time'
  $field_bases['field_event_display_time'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_event_display_time',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        0 => '',
        1 => '',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_boolean',
  );

  // Exported field_base: 'field_field_start_end_date_tz'
  $field_bases['field_field_start_end_date_tz'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_field_start_end_date_tz',
    'indexes' => array(),
    'locked' => 0,
    'module' => 'date',
    'settings' => array(
      'cache_count' => 4,
      'cache_enabled' => 1,
      'granularity' => array(
        'day' => 'day',
        'hour' => 'hour',
        'minute' => 'minute',
        'month' => 'month',
        'second' => 0,
        'year' => 'year',
      ),
      'timezone_db' => 'UTC',
      'todate' => 'optional',
      'tz_handling' => 'date',
    ),
    'translatable' => 0,
    'type' => 'datetime',
  );

  return $field_bases;
}
