<?php
/**
 * @file
 * ethical_events.features.defaultconfig.inc
 */

/**
 * Implements hook_defaultconfig_features().
 */
function ethical_events_defaultconfig_features() {
  return array(
    'ethical_events' => array(
      'user_default_permissions' => 'user_default_permissions',
    ),
  );
}

/**
 * Implements hook_defaultconfig_user_default_permissions().
 */
function ethical_events_defaultconfig_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create oe_event content'.
  $permissions['create oe_event content'] = array(
    'name' => 'create oe_event content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any oe_event content'.
  $permissions['delete any oe_event content'] = array(
    'name' => 'delete any oe_event content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own oe_event content'.
  $permissions['delete own oe_event content'] = array(
    'name' => 'delete own oe_event content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any oe_event content'.
  $permissions['edit any oe_event content'] = array(
    'name' => 'edit any oe_event content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own oe_event content'.
  $permissions['edit own oe_event content'] = array(
    'name' => 'edit own oe_event content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  return $permissions;
}
