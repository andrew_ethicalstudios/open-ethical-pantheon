<?php
/**
 * @file
 * ethical_taxonomy.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function ethical_taxonomy_taxonomy_default_vocabularies() {
  return array(
    'tag' => array(
      'name' => 'Group',
      'machine_name' => 'tag',
      'description' => 'A taxonomy of tags to structure your site! Not visible to your visitors.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
