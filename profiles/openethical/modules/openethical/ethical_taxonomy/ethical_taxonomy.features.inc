<?php
/**
 * @file
 * ethical_taxonomy.features.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies_alter().
 */
function ethical_taxonomy_taxonomy_default_vocabularies_alter(&$data) {
  if (isset($data['panopoly_categories'])) {
    $data['panopoly_categories']['name'] = 'Category'; /* WAS: 'Categories' */
  }
}
