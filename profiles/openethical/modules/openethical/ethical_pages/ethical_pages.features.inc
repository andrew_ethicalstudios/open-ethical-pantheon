<?php
/**
 * @file
 * ethical_pages.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ethical_pages_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "panelizer" && $api == "panelizer") {
    return array("version" => "1");
  }
  if ($module == "panels_mini" && $api == "panels_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function ethical_pages_node_info() {
  $items = array(
    'oe_main_page' => array(
      'name' => t('Main page'),
      'base' => 'node_content',
      'description' => t('A very important page to display to users on your site. Often used as the entrance/introduction to a section of your site with prominent links to other important content in that section.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'panopoly_page' => array(
      'name' => t('Content Page'),
      'base' => 'node_content',
      'description' => t('An important page to display to users on your site.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
