<?php
/**
 * @file
 * ethical_partners.facetapi_defaults.inc
 */

/**
 * Implements hook_facetapi_default_facet_settings().
 */
function ethical_partners_facetapi_default_facet_settings() {
  $export = array();

  $facet = new stdClass();
  $facet->disabled = FALSE; /* Edit this to true to make a default facet disabled initially */
  $facet->api_version = 1;
  $facet->name = 'search_api@openethical_blog::field_partner:title';
  $facet->searcher = 'search_api@openethical_blog';
  $facet->realm = '';
  $facet->facet = 'field_partner:title';
  $facet->enabled = FALSE;
  $facet->settings = array(
    'operator' => 'and',
    'hard_limit' => '-1',
    'dependencies' => array(
      'bundle' => 'none',
      'bundle_selected' => array(),
    ),
    'facet_mincount' => '1',
    'facet_missing' => '0',
    'flatten' => 0,
    'query_type' => 'term',
    'default_true' => TRUE,
    'facet_search_ids' => array(),
  );
  $export['search_api@openethical_blog::field_partner:title'] = $facet;

  $facet = new stdClass();
  $facet->disabled = FALSE; /* Edit this to true to make a default facet disabled initially */
  $facet->api_version = 1;
  $facet->name = 'search_api@openethical_blog:block:field_partner:title';
  $facet->searcher = 'search_api@openethical_blog';
  $facet->realm = 'block';
  $facet->facet = 'field_partner:title';
  $facet->enabled = TRUE;
  $facet->settings = array(
    'weight' => 0,
    'widget' => 'facetapi_multiselect',
    'filters' => array(),
    'active_sorts' => array(
      'active' => 'active',
      'display' => 'display',
      'count' => 0,
      'indexed' => 0,
    ),
    'sort_weight' => array(
      'active' => '-50',
      'count' => '-49',
      'display' => '-48',
      'indexed' => '0',
    ),
    'sort_order' => array(
      'active' => '3',
      'count' => '3',
      'display' => '4',
      'indexed' => '4',
    ),
    'empty_behavior' => 'none',
    'soft_limit' => '20',
    'nofollow' => 1,
    'show_expanded' => 0,
    'add_count' => 1,
    'remove_selected' => 0,
    'auto_submit' => 1,
    'empty_text' => array(
      'value' => '',
      'format' => 'panopoly_wysiwyg_text',
    ),
    'submit_realm' => 'Save and go back to realm settings',
  );
  $export['search_api@openethical_blog:block:field_partner:title'] = $facet;

  $facet = new stdClass();
  $facet->disabled = FALSE; /* Edit this to true to make a default facet disabled initially */
  $facet->api_version = 1;
  $facet->name = 'search_api@openethical_document_library::field_partner:title';
  $facet->searcher = 'search_api@openethical_document_library';
  $facet->realm = '';
  $facet->facet = 'field_partner:title';
  $facet->enabled = FALSE;
  $facet->settings = array(
    'operator' => 'and',
    'hard_limit' => '-1',
    'dependencies' => array(
      'bundle' => 'none',
      'bundle_selected' => array(),
    ),
    'facet_mincount' => '1',
    'facet_missing' => '0',
    'flatten' => 0,
    'query_type' => 'term',
    'default_true' => TRUE,
    'facet_search_ids' => array(),
  );
  $export['search_api@openethical_document_library::field_partner:title'] = $facet;

  $facet = new stdClass();
  $facet->disabled = FALSE; /* Edit this to true to make a default facet disabled initially */
  $facet->api_version = 1;
  $facet->name = 'search_api@openethical_document_library:block:field_partner:title';
  $facet->searcher = 'search_api@openethical_document_library';
  $facet->realm = 'block';
  $facet->facet = 'field_partner:title';
  $facet->enabled = TRUE;
  $facet->settings = array(
    'weight' => 0,
    'widget' => 'facetapi_multiselect',
    'filters' => array(),
    'active_sorts' => array(
      'active' => 'active',
      'display' => 'display',
      'count' => 0,
      'indexed' => 0,
    ),
    'sort_weight' => array(
      'active' => '-50',
      'count' => '-49',
      'display' => '-48',
      'indexed' => '0',
    ),
    'sort_order' => array(
      'active' => '3',
      'count' => '3',
      'display' => '4',
      'indexed' => '4',
    ),
    'empty_behavior' => 'none',
    'soft_limit' => '20',
    'nofollow' => 1,
    'show_expanded' => 0,
    'add_count' => 1,
    'remove_selected' => 0,
    'auto_submit' => 1,
    'empty_text' => array(
      'value' => '',
      'format' => 'panopoly_wysiwyg_text',
    ),
    'submit_realm' => 'Save and go back to realm settings',
  );
  $export['search_api@openethical_document_library:block:field_partner:title'] = $facet;

  $facet = new stdClass();
  $facet->disabled = FALSE; /* Edit this to true to make a default facet disabled initially */
  $facet->api_version = 1;
  $facet->name = 'search_api@openethical_image_library::field_partner:title';
  $facet->searcher = 'search_api@openethical_image_library';
  $facet->realm = '';
  $facet->facet = 'field_partner:title';
  $facet->enabled = FALSE;
  $facet->settings = array(
    'operator' => 'and',
    'hard_limit' => '-1',
    'dependencies' => array(
      'bundle' => 'none',
      'bundle_selected' => array(),
    ),
    'facet_mincount' => '1',
    'facet_missing' => '0',
    'flatten' => 0,
    'query_type' => 'term',
    'default_true' => TRUE,
    'facet_search_ids' => array(),
  );
  $export['search_api@openethical_image_library::field_partner:title'] = $facet;

  $facet = new stdClass();
  $facet->disabled = FALSE; /* Edit this to true to make a default facet disabled initially */
  $facet->api_version = 1;
  $facet->name = 'search_api@openethical_image_library:block:field_partner:title';
  $facet->searcher = 'search_api@openethical_image_library';
  $facet->realm = 'block';
  $facet->facet = 'field_partner:title';
  $facet->enabled = TRUE;
  $facet->settings = array(
    'weight' => 0,
    'widget' => 'facetapi_multiselect',
    'filters' => array(),
    'active_sorts' => array(
      'active' => 'active',
      'display' => 'display',
      'count' => 0,
      'indexed' => 0,
    ),
    'sort_weight' => array(
      'active' => '-50',
      'count' => '-49',
      'display' => '-48',
      'indexed' => '0',
    ),
    'sort_order' => array(
      'active' => '3',
      'count' => '3',
      'display' => '4',
      'indexed' => '4',
    ),
    'empty_behavior' => 'none',
    'soft_limit' => '20',
    'nofollow' => 1,
    'show_expanded' => 0,
    'add_count' => 1,
    'remove_selected' => 0,
    'auto_submit' => 1,
    'empty_text' => array(
      'value' => '',
      'format' => 'panopoly_wysiwyg_text',
    ),
    'submit_realm' => 'Save and go back to realm settings',
  );
  $export['search_api@openethical_image_library:block:field_partner:title'] = $facet;

  $facet = new stdClass();
  $facet->disabled = FALSE; /* Edit this to true to make a default facet disabled initially */
  $facet->api_version = 1;
  $facet->name = 'search_api@openethical_video_library::field_partner:title';
  $facet->searcher = 'search_api@openethical_video_library';
  $facet->realm = '';
  $facet->facet = 'field_partner:title';
  $facet->enabled = FALSE;
  $facet->settings = array(
    'operator' => 'and',
    'hard_limit' => '-1',
    'dependencies' => array(
      'bundle' => 'none',
      'bundle_selected' => array(),
    ),
    'facet_mincount' => '1',
    'facet_missing' => '0',
    'flatten' => 0,
    'query_type' => 'term',
    'default_true' => TRUE,
    'facet_search_ids' => array(),
  );
  $export['search_api@openethical_video_library::field_partner:title'] = $facet;

  $facet = new stdClass();
  $facet->disabled = FALSE; /* Edit this to true to make a default facet disabled initially */
  $facet->api_version = 1;
  $facet->name = 'search_api@openethical_video_library:block:field_partner:title';
  $facet->searcher = 'search_api@openethical_video_library';
  $facet->realm = 'block';
  $facet->facet = 'field_partner:title';
  $facet->enabled = TRUE;
  $facet->settings = array(
    'weight' => 0,
    'widget' => 'facetapi_multiselect',
    'filters' => array(),
    'active_sorts' => array(
      'active' => 'active',
      'display' => 'display',
      'count' => 0,
      'indexed' => 0,
    ),
    'sort_weight' => array(
      'active' => '-50',
      'count' => '-49',
      'display' => '-48',
      'indexed' => '0',
    ),
    'sort_order' => array(
      'active' => '3',
      'count' => '3',
      'display' => '4',
      'indexed' => '4',
    ),
    'empty_behavior' => 'none',
    'soft_limit' => '20',
    'nofollow' => 1,
    'show_expanded' => 0,
    'add_count' => 1,
    'remove_selected' => 0,
    'auto_submit' => 1,
    'empty_text' => array(
      'value' => '',
      'format' => 'panopoly_wysiwyg_text',
    ),
    'submit_realm' => 'Save and go back to realm settings',
  );
  $export['search_api@openethical_video_library:block:field_partner:title'] = $facet;

  return $export;
}
