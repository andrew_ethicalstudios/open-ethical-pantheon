<?php
/**
 * @file
 * ethical_partners.features.defaultconfig.inc
 */

/**
 * Implements hook_defaultconfig_features().
 */
function ethical_partners_defaultconfig_features() {
  return array(
    'ethical_partners' => array(
      'user_default_permissions' => 'user_default_permissions',
    ),
  );
}

/**
 * Implements hook_defaultconfig_user_default_permissions().
 */
function ethical_partners_defaultconfig_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create oe_partner content'.
  $permissions['create oe_partner content'] = array(
    'name' => 'create oe_partner content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any oe_partner content'.
  $permissions['delete any oe_partner content'] = array(
    'name' => 'delete any oe_partner content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own oe_partner content'.
  $permissions['delete own oe_partner content'] = array(
    'name' => 'delete own oe_partner content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any oe_partner content'.
  $permissions['edit any oe_partner content'] = array(
    'name' => 'edit any oe_partner content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own oe_partner content'.
  $permissions['edit own oe_partner content'] = array(
    'name' => 'edit own oe_partner content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  return $permissions;
}
