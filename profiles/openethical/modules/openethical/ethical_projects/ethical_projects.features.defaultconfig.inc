<?php
/**
 * @file
 * ethical_projects.features.defaultconfig.inc
 */

/**
 * Implements hook_defaultconfig_features().
 */
function ethical_projects_defaultconfig_features() {
  return array(
    'ethical_projects' => array(
      'user_default_permissions' => 'user_default_permissions',
    ),
  );
}

/**
 * Implements hook_defaultconfig_user_default_permissions().
 */
function ethical_projects_defaultconfig_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create oe_project content'.
  $permissions['create oe_project content'] = array(
    'name' => 'create oe_project content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any oe_project content'.
  $permissions['delete any oe_project content'] = array(
    'name' => 'delete any oe_project content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own oe_project content'.
  $permissions['delete own oe_project content'] = array(
    'name' => 'delete own oe_project content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any oe_project content'.
  $permissions['edit any oe_project content'] = array(
    'name' => 'edit any oe_project content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own oe_project content'.
  $permissions['edit own oe_project content'] = array(
    'name' => 'edit own oe_project content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  return $permissions;
}
