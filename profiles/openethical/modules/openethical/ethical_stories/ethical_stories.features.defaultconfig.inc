<?php
/**
 * @file
 * ethical_stories.features.defaultconfig.inc
 */

/**
 * Implements hook_defaultconfig_features().
 */
function ethical_stories_defaultconfig_features() {
  return array(
    'ethical_stories' => array(
      'user_default_permissions' => 'user_default_permissions',
    ),
  );
}

/**
 * Implements hook_defaultconfig_user_default_permissions().
 */
function ethical_stories_defaultconfig_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create fieldable oe_promo_with_section_content'.
  $permissions['create fieldable oe_promo_with_section_content'] = array(
    'name' => 'create fieldable oe_promo_with_section_content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'fieldable_panels_panes',
  );

  // Exported permission: 'create fieldable oe_quote_with_section_content'.
  $permissions['create fieldable oe_quote_with_section_content'] = array(
    'name' => 'create fieldable oe_quote_with_section_content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'fieldable_panels_panes',
  );

  // Exported permission: 'create fieldable oe_stand_out_text'.
  $permissions['create fieldable oe_stand_out_text'] = array(
    'name' => 'create fieldable oe_stand_out_text',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'fieldable_panels_panes',
  );

  // Exported permission: 'create oe_story content'.
  $permissions['create oe_story content'] = array(
    'name' => 'create oe_story content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any oe_story content'.
  $permissions['delete any oe_story content'] = array(
    'name' => 'delete any oe_story content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete fieldable oe_promo_with_section_content'.
  $permissions['delete fieldable oe_promo_with_section_content'] = array(
    'name' => 'delete fieldable oe_promo_with_section_content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'fieldable_panels_panes',
  );

  // Exported permission: 'delete fieldable oe_quote_with_section_content'.
  $permissions['delete fieldable oe_quote_with_section_content'] = array(
    'name' => 'delete fieldable oe_quote_with_section_content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'fieldable_panels_panes',
  );

  // Exported permission: 'delete fieldable oe_stand_out_text'.
  $permissions['delete fieldable oe_stand_out_text'] = array(
    'name' => 'delete fieldable oe_stand_out_text',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'fieldable_panels_panes',
  );

  // Exported permission: 'delete own oe_story content'.
  $permissions['delete own oe_story content'] = array(
    'name' => 'delete own oe_story content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any oe_story content'.
  $permissions['edit any oe_story content'] = array(
    'name' => 'edit any oe_story content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit fieldable oe_promo_with_section_content'.
  $permissions['edit fieldable oe_promo_with_section_content'] = array(
    'name' => 'edit fieldable oe_promo_with_section_content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'fieldable_panels_panes',
  );

  // Exported permission: 'edit fieldable oe_quote_with_section_content'.
  $permissions['edit fieldable oe_quote_with_section_content'] = array(
    'name' => 'edit fieldable oe_quote_with_section_content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'fieldable_panels_panes',
  );

  // Exported permission: 'edit fieldable oe_stand_out_text'.
  $permissions['edit fieldable oe_stand_out_text'] = array(
    'name' => 'edit fieldable oe_stand_out_text',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'fieldable_panels_panes',
  );

  // Exported permission: 'edit own oe_story content'.
  $permissions['edit own oe_story content'] = array(
    'name' => 'edit own oe_story content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'use text format plain_text_with_formatting'.
  $permissions['use text format plain_text_with_formatting'] = array(
    'name' => 'use text format plain_text_with_formatting',
    'roles' => array(),
    'module' => 'filter',
  );

  return $permissions;
}
