<?php
/**
* This plugin array is more or less self documenting
*/
$plugin = array(
  // the title in the admin
  'title' => t('Ethical stories nav pane'),
  // no one knows if "single" defaults to FALSE...
  'single' => TRUE,
  // oh joy, I get my own section of panel panes
  'category' => array(t('Admin'), -9),
  'render callback' => 'ethical_stories_nav_pane_content_type_render'
);


/**
* Run-time rendering of the body of the block (content type)
* See ctools_plugin_examples for more advanced info
*/
function ethical_stories_nav_pane_content_type_render($subtype, $conf, $context = NULL) {

  $output = '';
  $block = new stdClass();
  $nav_num = 0;
  $nid = $context[0];

  //if we couldn't get the nid from context, assume ajax call and attempt to get nid via the url
  if(!is_numeric($nid)){
    $uri = urldecode(check_plain($_SERVER['REQUEST_URI']));
    $uri_bits = explode(':', $uri);

    if(count($uri_bits) > 2){
      $nid = $uri_bits[2];
    }
  }


  if(is_numeric($nid)){

    $node = node_load($nid);

    $field_oe_story_section_titles = field_view_field('node', $node, 'field_oe_story_section_titles');

    if(!empty($field_oe_story_section_titles['#items'])){

      foreach ($field_oe_story_section_titles['#items'] as $num => $item) {

        $nav_num++;
        $class='';
        $title = $item['safe_value'];
        $story_id = transliteration_clean_filename('stripe' . ($num + 1) . '-' . $title);

        if($nav_num == 1){
          $class=' class="active"';
        }

        $output .= '<li><a href="#' . $story_id . '"' . $class . '>' . $title . '</a></li>';
      }

      if($output != ''){
        $output = '<div id="story-nav" class="hidden-xs"><ul class="nav">' . $output . '</ul></div>';
      }

    }

  }

  $block->content = $output;

  return $block;
}


