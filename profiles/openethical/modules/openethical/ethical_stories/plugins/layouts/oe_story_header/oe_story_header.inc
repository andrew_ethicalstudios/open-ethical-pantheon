<?php
// Plugin definition
$plugin = array(
  'title' => t('Open Ethical story header'),
  'icon' => 'oe-story-header.png',
  'category' => t('Open Ethical'),
  'theme' => 'oe_story_header',
  'css' => 'oe-story-header.css',
  'regions' => array(
    'contentmain' => t('Content'),
  ),
);
