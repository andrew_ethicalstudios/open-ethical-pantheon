<?php
/**
 * @file
 * ethical_stories.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ethical_stories_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "panelizer" && $api == "panelizer") {
    return array("version" => "1");
  }
  if ($module == "panels_mini" && $api == "panels_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function ethical_stories_image_default_styles() {
  $styles = array();

  // Exported image style: oe_story_banner.
  $styles['oe_story_banner'] = array(
    'label' => 'Story banner',
    'effects' => array(
      5 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 1400,
          'height' => 560,
          'upscale' => 1,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'oe_story_banner',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: oe_tiny_profile.
  $styles['oe_tiny_profile'] = array(
    'label' => 'Tiny profile',
    'effects' => array(
      6 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 75,
          'height' => 75,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'oe_tiny_profile',
        ),
        'weight' => 0,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function ethical_stories_node_info() {
  $items = array(
    'oe_story' => array(
      'name' => t('Story'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
