<?php
/**
 * @file
 * ethical_stories.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function ethical_stories_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_promo|fieldable_panels_pane|oe_promo_with_section_content|form';
  $field_group->group_name = 'group_promo';
  $field_group->entity_type = 'fieldable_panels_pane';
  $field_group->bundle = 'oe_promo_with_section_content';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Promo',
    'weight' => '1',
    'children' => array(
      0 => 'field_oe_promo_content',
      1 => 'field_oe_promo_title',
      2 => 'field_oe_promo_image',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-promo field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_promo|fieldable_panels_pane|oe_promo_with_section_content|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_quote|fieldable_panels_pane|oe_quote_with_section_content|default';
  $field_group->group_name = 'group_quote';
  $field_group->entity_type = 'fieldable_panels_pane';
  $field_group->bundle = 'oe_quote_with_section_content';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Quote',
    'weight' => '1',
    'children' => array(
      0 => 'field_oe_quote_content',
      1 => 'field_oe_quote_name',
      2 => 'field_oe_quote_role',
      3 => 'field_oe_quote_image',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-quote field-group-fieldset',
      ),
    ),
  );
  $export['group_quote|fieldable_panels_pane|oe_quote_with_section_content|default'] = $field_group;

  return $export;
}
