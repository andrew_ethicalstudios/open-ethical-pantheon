<?php
/**
 * @file
 * ethical_stories.features.metatag.inc
 */

/**
 * Implements hook_metatag_export_default().
 */
function ethical_stories_metatag_export_default() {
  $config = array();

  // Exported Metatag config instance: node:oe_story.
  $config['node:oe_story'] = array(
    'instance' => 'node:oe_story',
    'config' => array(
      'image_src' => array(
        'value' => '[node:field_featured_image]',
      ),
      'og:image' => array(
        'value' => '[node:field_featured_image]',
      ),
    ),
  );

  return $config;
}
