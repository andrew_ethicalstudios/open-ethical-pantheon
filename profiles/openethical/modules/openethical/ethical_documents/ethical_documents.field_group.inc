<?php
/**
 * @file
 * ethical_documents.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function ethical_documents_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_categories|file|document|form';
  $field_group->group_name = 'group_categories';
  $field_group->entity_type = 'file';
  $field_group->bundle = 'document';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Edit tags',
    'weight' => '4',
    'children' => array(
      0 => 'field_country',
      1 => 'field_featured_categories',
      2 => 'field_group',
      3 => 'field_project',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Categories',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => ' group-categories field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_categories|file|document|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_details|file|document|form';
  $field_group->group_name = 'group_details';
  $field_group->entity_type = 'file';
  $field_group->bundle = 'document';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Edit details',
    'weight' => '3',
    'children' => array(
      0 => 'field_featured_image',
      1 => 'field_file_author',
      2 => 'field_file_copyright',
      3 => 'field_file_description',
      4 => 'field_file_published_date',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Details',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => ' group-details field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_details|file|document|form'] = $field_group;

  return $export;
}
