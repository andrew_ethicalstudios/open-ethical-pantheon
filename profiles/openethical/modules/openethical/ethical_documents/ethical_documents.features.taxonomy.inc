<?php
/**
 * @file
 * ethical_documents.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function ethical_documents_taxonomy_default_vocabularies() {
  return array(
    'oe_document_type' => array(
      'name' => 'Document type',
      'machine_name' => 'oe_document_type',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
