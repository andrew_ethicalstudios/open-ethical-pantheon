<?php
/**
 * @file
 * ethical_media.features.defaultconfig.inc
 */

/**
 * Implements hook_defaultconfig_features().
 */
function ethical_media_defaultconfig_features() {
  return array(
    'ethical_media' => array(
      'user_default_permissions' => 'user_default_permissions',
    ),
  );
}

/**
 * Implements hook_defaultconfig_user_default_permissions().
 */
function ethical_media_defaultconfig_user_default_permissions() {
  $permissions = array();



  return $permissions;
}
