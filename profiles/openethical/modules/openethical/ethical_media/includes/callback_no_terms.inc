<?php

/**
 * Ethical media data alteration callback that filters out media that have not been tagged
 *
 */
class EthicalMediaAlterNoTerms extends SearchApiAbstractAlterCallback {

  public function alterItems(array &$items) {
    foreach ($items as $id => $item) {
      if((count($item->field_featured_categories) == 0) && (count($item->field_country) == 0) && (count($item->field_group) == 0) && (count($item->field_project) == 0)){
        unset($items[$id]);
      }
    }
  }
}
