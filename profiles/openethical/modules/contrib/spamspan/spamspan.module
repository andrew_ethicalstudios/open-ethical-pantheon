<?php

/**
 * @file
 * This module implements the spamspan technique (http://www.spamspan.com ) for hiding email addresses from spambots.
 *
 * If javascript is disabled on the client-side, addresses appear as
 * example [at] example [dot] com.
 *
 * @author Lawrence Akka
 * @copyright 2006-2010, Lawrence Akka
 * @license http://www.gnu.org/licenses/gpl.txt  GPL v 2.0
 *
 */

/**
 * @file
 * This module implements the spamspan technique (http://www.spamspan.com ) for hiding email addresses from spambots.
 *
 * Move less frequently used code out of the .module file.
 */

// TODO: refactor code to drop spamspan_admin class
class spamspan_admin {
  protected $configuration_page = 'admin/config/content/formats/spamspan';
  protected $defaults;
  protected $display_name = 'SpamSpan';
  protected $filter;
  protected $help = '<p>The SpamSpan module obfuscates email addresses to help prevent spambots from collecting them. Read the <a href="@url">Spamspan configuration page</a>.</p>';
  protected $page;

  function __construct() {
    $info = spamspan_filter_info();
    $this->defaults = $info['spamspan']['default settings'];
  }
  function defaults() {
    return $this->defaults;
  }
  function display_name() {
    return $this->display_name;
  }
  function filter_is() {
    return isset($this->filter);
  }
  function filter_set($filter) {
    $this->filter = $filter;
  }

  /**
   * Settings callback for spamspan filter
   */
  function filter_settings($form, $form_state, $filter, $format, $defaults, $filters) {
    $filter->settings += $defaults;

    // spamspan '@' replacement
    $settings['spamspan_at'] = array(
      '#type' => 'textfield',
      '#title' => t('Replacement for "@"'),
      '#default_value' => $filter->settings['spamspan_at'],
      '#required' => TRUE,
      '#description' => t('Replace "@" with this text when javascript is disabled.'),
    );
    $settings['spamspan_use_graphic'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use a graphical replacement for "@"'),
      '#default_value' => $filter->settings['spamspan_use_graphic'],
      '#description' => t('Replace "@" with a graphical representation when javascript is disabled'
        . ' (and ignore the setting "Replacement for @" above).'),
    );
    $settings['spamspan_dot_enable'] = array(
      '#type' => 'checkbox',
      '#title' => t('Replace dots in email with text'),
      '#default_value' => $filter->settings['spamspan_dot_enable'],
      '#description' => t('Switch on dot replacement.'),
    );
    $settings['spamspan_dot'] = array(
      '#type' => 'textfield',
      '#title' => t('Replacement for "."'),
      '#default_value' => $filter->settings['spamspan_dot'],
      '#required' => TRUE,
      '#description' => t('Replace "." with this text.'),
    );
    $settings['use_form'] = array(
      '#type' => 'fieldset',
      '#title' => t('Use a form instead of a link'),
    );
    $settings['use_form']['spamspan_use_form'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use a form instead of a link'),
      '#default_value' => $filter->settings['spamspan_use_form'],
      '#description' => t('Link to a contact form instad of an email address. The following settings are used only if you select this option.'),
    );
    $settings['use_form']['spamspan_form_pattern'] = array(
      '#type' => 'textfield',
      '#title' => t('Replacement string for the email address'),
      '#default_value' => $filter->settings['spamspan_form_pattern'],
      '#required' => TRUE,
      '#description' => t('Replace the email link with this string and substitute the following <br />%url = the url where the form resides,<br />%email = the email address (base64 and urlencoded),<br />%displaytext = text to display instead of the email address.'),
    );
    $settings['use_form']['spamspan_form_default_url'] = array(
      '#type' => 'textfield',
      '#title' => t('Default url'),
      '#default_value' => $filter->settings['spamspan_form_default_url'],
      '#required' => TRUE,
      '#description' => t('Default url to form to use if none specified (e.g. me@example.com[custom_url_to_form])'),
    );
    $settings['use_form']['spamspan_form_default_displaytext'] = array(
      '#type' => 'textfield',
      '#title' => t('Default displaytext'),
      '#default_value' => $filter->settings['spamspan_form_default_displaytext'],
      '#required' => TRUE,
      '#description' => t('Default displaytext to use if none specified (e.g. me@example.com[custom_url_to_form|custom_displaytext])'),
    );

    // we need this to insert our own validate/submit handlers
    // we use our own validate handler to extract use_form settings
    $settings['use_form']['#process'] = array('_spamspan_admin_settings_form_process');
    return $settings;
  }


  /**
   * Responds to hook_help().
   */
  function help($path, $arg) {
    switch ($path) {
      case 'admin/help#spamspan':
        return t($this->help, array('@url' => $this->configuration_page));
    }
  }

  /**
   * @function
   * Generic logging function. Used mainly for development.
   */
  function log($message, $variables = array()) {
    watchdog($this->display_name, $message, $variables);
  }
  /**
   * Responds to hook_menu().
   */
  function menu() {
    $items[$this->configuration_page] = array(
      'title' => 'Spamspan',
      'description' => 'Experiment with the Spamspan function.',
      'type' => MENU_LOCAL_TASK,
      'page callback' => 'drupal_get_form',
      'page arguments' => array('spamspan_admin_page'),
      'access arguments' => array('administer filters'),
    );
    return $items;
  }

  /**
   * A helper function for the callbacks
   *
   * Replace an email addresses which has been found with the appropriate
   * <span> tags
   *
   * @param $name
   *  The user name
   * @param $domain
   *  The email domain
   * @param $contents
   *  The contents of any <a> tag
   * @param $headers
   *  The email headers extracted from a mailto: URL
   * @param $vars
   *  Optional parameters to be implemented later.
   * @param $settings
   *  Provide specific settings. They replace anything set through filter_set().
   * @return
   *  The span with which to replace the email address
   */
  function output($name, $domain, $contents = '', $headers = array(), $vars = array(), $settings = NULL) {
    if ($settings === NULL) {
      $settings = $this->defaults;
      if ($this->filter_is()) {
        $settings = $this->filter->settings;
      }
    }

    // processing for forms
    if (!empty($settings['spamspan_use_form'])) {
      $email = urlencode(base64_encode($name . '@' . $domain));

      //put in the defaults if nothing set
      if (empty($vars['custom_form_url'])) {
        $vars['custom_form_url'] = $settings['spamspan_form_default_url'];
      }
      if (empty($vars['custom_displaytext'])) {
        $vars['custom_displaytext'] = t($settings['spamspan_form_default_displaytext']);
      }
      $vars['custom_form_url'] = strip_tags($vars['custom_form_url']);
      $vars['custom_displaytext'] = strip_tags($vars['custom_displaytext']);

      $url_parts = parse_url($vars['custom_form_url']);
      if (!$url_parts) {
        $vars['custom_form_url'] = '';
      }
      else if (empty($url_parts['host'])) {
        $vars['custom_form_url'] = base_path() . trim($vars['custom_form_url'], '/');
      }

      $replace = array('%url' => $vars['custom_form_url'], '%displaytext' => $vars['custom_displaytext'], '%email' => $email);

      $output = strtr($settings['spamspan_form_pattern'], $replace);
      return $output;
    }

    $at = $settings['spamspan_at'];
    if ($settings['spamspan_use_graphic']) {
      $at = theme('spamspan_at_sign', array('settings' => $settings));
    }

    if ($settings['spamspan_dot_enable']) {
      // Replace .'s in the address with [dot]
      $name = str_replace('.', '<span class="t">' . $settings['spamspan_dot'] . '</span>', $name);
      $domain = str_replace('.', '<span class="t">' . $settings['spamspan_dot'] . '</span>', $domain);
    }
    $output = '<span class="u">' . $name . '</span>' . $at . '<span class="d">' . $domain . '</span>';


    // if there are headers, include them as eg (subject: xxx, cc: zzz)
    // we replace the = in the headers by ": " to look nicer
    if (count($headers)) {
      foreach ($headers as $key => $header) {
        // check if header is already urlencoded, if not, encode it
        if ($header == rawurldecode($header)) {
          $header = rawurlencode($header);
          //replace the first = sign
          $header = preg_replace('/%3D/', ': ', $header, 1);
        }
        else {
          $header = str_replace('=', ': ', $header);
        }

        $headers[$key] = $header;
      }
      $output .= '<span class="h"> (' . check_plain(implode(', ', $headers)) . ') </span>';
    }

    // If there are tag contents, include them, between round brackets.
    // Remove emails from the tag contents, otherwise the tag contents are themselves
    // converted into a spamspan, with undesirable consequences - see bug #305464.
    if (!empty($contents)) {
      $contents = preg_replace('!' . SPAMSPAN_EMAIL . '!ix', '', $contents);

      // remove anything except certain inline elements, just in case.  NB nested
      // <a> elements are illegal.
      $contents = filter_xss($contents, array('em', 'strong', 'cite', 'b', 'i', 'code', 'span', 'img'));

      if (!empty($contents)) {
        $output .= '<span class="a"> (' . $contents . ')</span>';
      }
    }

    // put in the extra <a> attributes
    // this has to come after the xss filter, since we want comment tags preserved
    if (!empty($vars['extra_attributes'])) {
      $output .= '<span class="e"><!--'. strip_tags($vars['extra_attributes']) .'--></span>';
    }

    $output = '<span class="spamspan">' . $output . '</span>';

    return $output;
  }

  /**
   * Return the admin page.
   * External text should be checked: = array('#markup' => check_plain($format->name));
   */
  function page_object() {
    if (!isset($this->page)) {
      $this->page = new spamspan_admin_page($this);
    }
    return $this->page;
  }
  function page($form, &$form_state) {
    return $this->page_object()->form($form, $form_state);
  }
  function page_submit($form, &$form_state) {
    $this->page_object()->submit($form, $form_state);
  }
}

function _spamspan_admin_settings_form_process(&$element, &$form_state, &$complete_form) {
  $complete_form['#validate'][] = '_spamspan_admin_settings_form_validate';
  return $element;
}

function _spamspan_admin_settings_form_validate(&$form, &$form_state) {
  $settings = $form_state['values']['filters']['spamspan']['settings'];
  $use_form = $settings['use_form'];

  //no trees, see https://www.drupal.org/node/2378437
  unset($settings['use_form']);
  $settings += $use_form;
  $form_state['values']['filters']['spamspan']['settings'] = $settings;
}

/**
 *  Set up a regex constant to split an email address into name and domain
 *  parts. The following pattern is not perfect (who is?), but is intended to
 * intercept things which look like email addresses.  It is not intended to
 * determine if an address is valid.  It will not intercept addresses with
 * quoted local parts.
 *
 * @constant string SPAMSPAN_EMAIL
 */
define('SPAMSPAN_EMAIL', "
      ([-\.\~\'\!\#\$\%\&\+\/\*\=\?\^\_\`\{\|\}\w\+^@]+) # Group 1 - Match the name part - dash, dot or
                           #special characters.
     @                     # @
     ((?:        # Group 2
       [-\w]+\.            # one or more letters or dashes followed by a dot.
       )+                  # The whole thing one or more times
       [A-Z]{2,63}         # with between 2 and 63 letters at the end (NB
                           # new TLDs)
     )
");

/**
 * Loads code used only in admin.
 */
function spamspan_admin() {
  static $object;
  if (!isset($object)) {
    $object = new spamspan_admin();
  }
  return $object;
}

/**
 * menu(): 'page arguments' => array('spamspan_admin_page'),
 */
function spamspan_admin_page($form, &$form_state) {
  watchdog('spamspan', 'admin_page()');
  return spamspan_admin()->page($form, $form_state);
}
/**
 * .
 */
function spamspan_admin_page_submit($form, &$form_state) {
  watchdog('spamspan', 'admin_page_submit()');
  return spamspan_admin()->page_submit($form, $form_state);
}

/**
 * Implements hook_filter_info().
 * This function is called on every page so keep it fast and simple.
 */
function spamspan_filter_info() {
  $filters['spamspan']   = array(
    'title' => t('SpamSpan email address encoding filter'),
    'description' => t('Attempt to hide email addresses from spam-bots.'),
    'process callback' => '_spamspan_filter_process',
    'settings callback' => '_spamspan_filter_settings',
    'tips callback' => '_spamspan_filter_tips',
    'default settings' => array(
      'spamspan_at' => ' [at] ',
      'spamspan_use_graphic' => 0,
      'spamspan_dot_enable' => 0,
      'spamspan_dot' => ' [dot] ',
      'spamspan_use_form' => 0,
      'spamspan_form_pattern' => '<a href="%url?goto=%email">%displaytext</a>',
      'spamspan_form_default_url' => 'contact',
      'spamspan_form_default_displaytext' => 'contact form'
    ),
  );
  return $filters;
}

/**
 * Spamspan filter process callback
 *
 * Scan text and replace email addresses with span tags
 *
 * We are aiming to replace emails with code like this:
 *   <span class="spamspan">
 *   <span class="u">user</span>
 *   [at]
 *   <span class="d">example [dot] com</span>
 *   <span class="t"tag contents></span></span>
 *
 */
function _spamspan_filter_process($text, $filter) {
  // The preg_replace_callback functions below cannot take any additional
  // arguments, so we pass the relevant settings in spamspan_admin.
  spamspan_admin()->filter_set($filter);
  // Top and tail the email regexp it so that it is case insensitive and
  // ignores whitespace.
  $emailpattern = '!'. SPAMSPAN_EMAIL .'!ix';
  $emailpattern_with_options = '!'. SPAMSPAN_EMAIL .'\[(.*?)\]!ix';

  // Next set up a regex for mailto: URLs.
  // - see http://www.faqs.org/rfcs/rfc2368.html
  // This captures the whole mailto: URL into the second group,
  // the name into the third group and the domain into
  // the fourth. The tag contents go into the fifth.

  $mailtopattern = "!<a\s+                                # opening <a and spaces
      ((?:\w+\s*=\s*)(?:\w+|\"[^\"]*\"|'[^']*'))*?        # any attributes
      \s*                                                 # whitespace
      href\s*=\s*(['\"])(mailto:"                         # the href attribute
      . SPAMSPAN_EMAIL .                                  # The email address
      "(?:\?[A-Za-z0-9_= %\.\-\~\_\&;\!\*\(\)\'#&]*)?)" . # an optional ? followed
                                                          # by a query string. NB
                                                          # we allow spaces here,
                                                          # even though strictly
                                                          # they should be URL
                                                          # encoded
      "\\2                                                # the relevant quote
                                                          # character
      ((?:\s+\w+\s*=\s*)(?:\w+|\"[^\"]*\"|'[^']*'))*?     # any more attributes
      >                                                   # end of the first tag
      (.*?)                                               # tag contents.  NB this
                                                          # will not work properly
                                                          # if there is a nested
                                                          # <a>, but this is not
                                                          # valid xhtml anyway.
      </a>                                                # closing tag
      !ix";

  // HTML image tags need to be handled separately, as they may contain base64
  // encoded images slowing down the email regex function.
  // Therefore, remove all image contents and add them back later.
  // See https://drupal.org/node/1243042 for details.
  $images = array(array());
  $inline_image_pattern = '/data\:(?:.+?)base64(?:.+?)["|\']/';
  preg_match_all($inline_image_pattern, $text, $images);
  $text = preg_replace($inline_image_pattern, '__spamspan_img_placeholder__', $text);

  // Now we can convert all mailto URLs
  $text = preg_replace_callback($mailtopattern, '_spamspan_callback_mailto', $text);
  // all bare email addresses with optional formatting information
  $text = preg_replace_callback($emailpattern_with_options, '_spamspan_email_addresses_with_options', $text);
  // and finally, all bare email addresses
  $text = preg_replace_callback($emailpattern, '_spamspan_bare_email_addresses', $text);

  // Revert back to the original image contents.
  foreach ($images[0] as $image) {
    $text = preg_replace('/__spamspan_img_placeholder__/', $image, $text, 1);
  }
  return $text;
}

function _spamspan_email_addresses_with_options($matches) {
  $vars = array();
  if (!empty($matches[3])) {
    $options = explode('|', $matches[3]);
    if (!empty($options[0])) {
      $custom_form_url = trim($options[0]);
      if (!empty($custom_form_url)) {
        $vars['custom_form_url'] = $custom_form_url;
      }
    }
    if (!empty($options[1])) {
      $custom_displaytext = trim($options[1]);
      if (!empty($custom_displaytext)) {
        $vars['custom_displaytext'] = $custom_displaytext;
      }
    }
  }
  return spamspan_admin()->output($matches[1], $matches[2], '', '', $vars);
}

function _spamspan_bare_email_addresses($matches) {
  return spamspan_admin()->output($matches[1], $matches[2]);
}

/**
 * Settings callback for spamspan filter
 */
function _spamspan_filter_settings($form, $form_state, $filter, $format, $defaults, $filters) {
  return spamspan_admin()->filter_settings($form, $form_state, $filter, $format, $defaults, $filters);
}

/**
 * Filter tips callback
 */
function _spamspan_filter_tips($filter, $format, $long = FALSE) {
  return t('Each email address will be obfuscated in a human readable fashion or, if JavaScript is enabled, replaced with a spam resistent clickable link.'
    . ' Email addresses will get the default web form unless specified. If replacement text (a persons name) is required a webform is also required. Separate each part with the "|" pipe symbol. Replace spaces in names with "_".');
}

/**
 * Implements hook_help().
 */
function spamspan_help($path, $arg) {
  switch ($path) {
    case 'admin/help#spamspan':
      return spamspan_admin()->help($path, $arg);
  }
}

/**
 * The callback functions for preg_replace_callback
 *
 * Replace an email addresses which has been found with the appropriate
 * <span> tags
 *
 * @param $matches
 *  An array containing parts of an email address or mailto: URL.
 * @return
 *  The span with which to replace the email address
 */
function _spamspan_callback_mailto($matches) {
  // take the mailto: URL in $matches[3] and split the query string
  // into its component parts, putting them in $headers as
  // [0]=>"header=contents" etc.  We cannot use parse_str because
  // the query string might contain dots.

  // Single quote can be encoded as &#039; which breaks parse_url
  // Replace it back to a single quote which is perfectly valid
  $matches[3] = str_replace("&#039;", '\'', $matches[3]);
  $query = parse_url($matches[3], PHP_URL_QUERY);
  $query = str_replace('&amp;', '&', $query);
  $headers = preg_split('/[&;]/', $query);
  // if no matches, $headers[0] will be set to '' so $headers must be reset
  if ($headers[0] == '') {
    $headers = array();
  }

  // take all <a> attributes except the href and put them into custom $vars
  $vars = $attributes = array();
  //before href
  if (!empty($matches[1])) {
    $matches[1] = trim($matches[1]);
    $attributes[] = $matches[1];
  }
  //after href
  if (!empty($matches[6])) {
    $matches[6] = trim($matches[6]);
    $attributes[] = $matches[6];
  }

  if (count($attributes)) {
    $vars['extra_attributes'] = implode(' ', $attributes);
  }

  return spamspan_admin()->output($matches[4], $matches[5], $matches[7], $headers, $vars);
}

/**
 * Implements hook_menu().
 */
function spamspan_menu() {
  $items = spamspan_admin()->menu();
  return $items;
}

/**
 * A simple utility function wrapping the main processing callback.
 * This function may be called by other modules and themes.
 *
 * @param $text
 *  Text, maybe containing email addresses.
 * @param $settings
 * An associative array of settings to be applied.
 * @return
 *  The input text with emails replaced by spans
 */
function spamspan($text = '', $settings = array()) {
  //apply default settings
  $info = spamspan_filter_info();
  $defaults = $info['spamspan']['default settings'];
  //create a dummy filter object so we can apply the settings
  $filter = new stdClass();
  $filter->settings = $settings + $defaults;
  return _spamspan_filter_process($text, $filter);
}

/**
 * Implements hook_theme().
 */
function spamspan_theme($existing, $type, $theme, $path) {
  return array(
    'spamspan_at_sign' => array(
      'variables' => array(
        // the spamspan filter settings if anyone needs them
        'settings' => array(),
      ),
    ),
  );
}

function theme_spamspan_at_sign($variables) {
  $output = '<img class="spam-span-image" alt="at" width="10" src="' . base_path() . drupal_get_path('module', 'spamspan') . '/image.gif" />';
  return $output;
}
