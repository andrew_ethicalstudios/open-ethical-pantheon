<?php

/**
 * @file
 * Country translation handler for the entity translation module.
 */


/**
 * Country translation handler.
 */
class EntityTranslationCountryHandler extends EntityTranslationDefaultHandler {

  public function __construct($entity_type, $entity_info, $entity) {
    parent::__construct('country', $entity_info, $entity);
  }

  /**
   * @see EntityTranslationDefaultHandler::entityForm()
   */
  public function entityForm(&$form, &$form_state) {
    parent::entityForm($form, $form_state);
    // Adjust the translation fieldset weight to move it below the admin one.
    $form['translation']['#weight'] = 99;
    $form['actions']['#weight'] = 100;
  }

  /**
   * @see EntityTranslationDefaultHandler::entityFormLanguageWidgetSubmit()
   */
  public function entityFormLanguageWidgetSubmit($form, &$form_state) {
    $this->updateFormLanguage($form_state);
  }

  /**
   * @see EntityTranslationDefaultHandler::entityFormTitle()
   */
  protected function entityFormTitle() {
    return t('Edit country @name', array('@name' => $this->getLabel()));
  }

  /**
   * @see EntityTranslationDefaultHandler::getStatus()
   */
  protected function getStatus() {
    return (boolean) $this->entity->status;
  }
}
