<?php
/**
 * @file
 * client_translation_overrides.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function client_translation_overrides_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: field_base
  $overrides["field_base.body.settings|entity_translation_sync"] = FALSE;
  $overrides["field_base.body.translatable"] = 1;
  $overrides["field_base.field_basic_file_text.settings|entity_translation_sync"] = FALSE;
  $overrides["field_base.field_basic_file_text.translatable"] = 1;
  $overrides["field_base.field_basic_image_caption.settings|entity_translation_sync"] = FALSE;
  $overrides["field_base.field_basic_image_caption.translatable"] = 1;
  $overrides["field_base.field_basic_spotlight_items.settings|entity_translation_sync"] = FALSE;
  $overrides["field_base.field_basic_spotlight_items.translatable"] = 1;
  $overrides["field_base.field_basic_text_text.settings|entity_translation_sync"] = FALSE;
  $overrides["field_base.field_basic_text_text.translatable"] = 1;
  $overrides["field_base.field_custom_link_title.settings|entity_translation_sync"] = FALSE;
  $overrides["field_base.field_custom_link_title.translatable"] = 1;
  $overrides["field_base.field_custom_text.settings|entity_translation_sync"] = FALSE;
  $overrides["field_base.field_custom_text.translatable"] = 1;
  $overrides["field_base.field_custom_title.settings|entity_translation_sync"] = FALSE;
  $overrides["field_base.field_custom_title.translatable"] = 1;
  $overrides["field_base.field_file_author.settings|entity_translation_sync"] = FALSE;
  $overrides["field_base.field_file_author.translatable"] = 1;
  $overrides["field_base.field_file_copyright.settings|entity_translation_sync"] = FALSE;
  $overrides["field_base.field_file_copyright.translatable"] = 1;
  $overrides["field_base.field_file_description.settings|entity_translation_sync"] = FALSE;
  $overrides["field_base.field_file_description.translatable"] = 1;
  $overrides["field_base.field_file_image_alt_text.settings|entity_translation_sync"] = FALSE;
  $overrides["field_base.field_file_image_alt_text.translatable"] = 1;
  $overrides["field_base.field_file_image_title_text.settings|entity_translation_sync"] = FALSE;
  $overrides["field_base.field_file_image_title_text.translatable"] = 1;
  $overrides["field_base.field_map_information.settings|entity_translation_sync"] = FALSE;
  $overrides["field_base.field_map_information.translatable"] = 1;

  // Exported overrides for: menu_custom
  $overrides["menu_custom.menu-secondary-menu.i18n_mode"] = 5;
  $overrides["menu_custom.menu-secondary-menu.language"] = 'und';
  $overrides["menu_custom.menu-tertiary-menu.i18n_mode"] = 5;
  $overrides["menu_custom.menu-tertiary-menu.language"] = 'und';

  // Exported overrides for: page_manager_pages
  $overrides["page_manager_pages.openethical_image_library.default_handlers|page_openethical_image_library_panel_context|conf|display|panel_settings|style_settings|footer"] = NULL;
  $overrides["page_manager_pages.openethical_video_library.default_handlers|page_openethical_video_library_panel_context|conf|display|panel_settings|style_settings|footer"] = NULL;

  // Exported overrides for: panelizer_defaults
  $overrides["panelizer_defaults.node:oe_blog_post:default.display|title"] = '%node:title_field';
  $overrides["panelizer_defaults.node:oe_event:default.display|title"] = '%node:title_field';
  $overrides["panelizer_defaults.node:oe_main_page:default.display|title"] = '%node:title_field';
  $overrides["panelizer_defaults.node:oe_project:default.display|title"] = '%node:title_field';
  $overrides["panelizer_defaults.node:panopoly_page:default.display|title"] = '%node:title_field';

  // Exported overrides for: search_api_index
  $overrides["search_api_index.openethical_blog.enabled"] = 0;
  $overrides["search_api_index.openethical_document_library.enabled"] = 0;
  $overrides["search_api_index.openethical_events.enabled"] = 0;
  $overrides["search_api_index.openethical_image_library.options|fields|field_oe_file_language"] = array(
    'type' => 'string',
  );
  $overrides["search_api_index.openethical_projects.enabled"] = 0;
  $overrides["search_api_index.openethical_video_library.options|fields|field_oe_file_language"] = array(
    'type' => 'string',
  );

  // Exported overrides for: views_view
  $overrides["views_view.openethical_image_gallery.display|default|display_options|filters|field_oe_file_language_value"] = array(
    'id' => 'field_oe_file_language_value',
    'table' => 'field_data_field_oe_file_language',
    'field' => 'field_oe_file_language_value',
    'value' => array(
      'und' => 'und',
    ),
    'group' => 1,
    'expose' => array(
      'operator_id' => 'field_oe_file_language_value_op',
      'label' => 'Image language',
      'operator' => 'field_oe_file_language_value_op',
      'identifier' => 'field_oe_file_language_value',
      'multiple' => TRUE,
      'remember_roles' => array(
        2 => 2,
        1 => 0,
        3 => 0,
        4 => 0,
      ),
    ),
  );
  $overrides["views_view.openethical_image_gallery.display|default|display_options|filters|field_oe_file_language_value_1"] = array(
    'id' => 'field_oe_file_language_value_1',
    'table' => 'field_data_field_oe_file_language',
    'field' => 'field_oe_file_language_value',
    'operator' => 'not empty',
    'group' => 2,
  );
  $overrides["views_view.openethical_video_gallery.display|default|display_options|filters|field_oe_file_language_value"] = array(
    'id' => 'field_oe_file_language_value',
    'table' => 'field_data_field_oe_file_language',
    'field' => 'field_oe_file_language_value',
    'value' => array(
      'und' => 'und',
    ),
    'group' => 1,
    'expose' => array(
      'operator_id' => 'field_oe_file_language_value_op',
      'label' => 'Video language',
      'operator' => 'field_oe_file_language_value_op',
      'identifier' => 'field_oe_file_language_value',
      'multiple' => TRUE,
      'remember_roles' => array(
        2 => 2,
        1 => 0,
        3 => 0,
        4 => 0,
      ),
    ),
  );
  $overrides["views_view.openethical_video_gallery.display|default|display_options|filters|field_oe_file_language_value_1"] = array(
    'id' => 'field_oe_file_language_value_1',
    'table' => 'field_data_field_oe_file_language',
    'field' => 'field_oe_file_language_value',
    'operator' => 'not empty',
    'group' => 2,
  );

 return $overrides;
}
