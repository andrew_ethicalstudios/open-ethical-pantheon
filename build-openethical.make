api = 2
core = 7.x

; MAKE file for Open Ethical used by Drupal.org packaging script

; Include the definition for how to build Drupal core directly, including patches:
includes[] = drupal-org-core.make

; Download the OpenEthical install profile and recursively build all its dependencies:
projects[openethical][type] = profile
projects[openethical][download][type] = git
projects[openethical][download][url] = "https://andrew_ethicalstudios@bitbucket.org/ethicalstudios/open-ethical-profile.git"
projects[openethical][download][branch] = "master"
